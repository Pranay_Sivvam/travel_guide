import React, { useState } from 'react';
import './placesincountries.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';

function Munnar(args) {
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const book = () => alert("Booking Successful")

    const imageUrls = [
        'https://www.holidify.com/images/bgImages/MUNNAR.jpg',
    ];
    return (
        <div>
            {/* Images */}
            <div className='topdata'>
                <div className="image-container">
                    <h1>Munnar</h1> <br></br>
                    {imageUrls.map((url, index) => (
                        <img key={index} src={url} alt={`Image ${index + 1}`}
                            style={{ width: '50%', height: '5%' }} />
                    ))}
                </div>
            </div>

            <br></br>

            {/* Data in points */}
            <div className='locationdata'>
                <h5><i class="fa-solid fa-location-dot"></i> Kerala - India</h5>
                <h5><i class="fa-solid fa-indian-rupee-sign"></i> Packages :  8,000 onwards </h5>
                <h5><i class="fa-regular fa-star"></i> Rating :  4.2 /5 (2023 ratings)</h5>
                <h5><i class="fa-solid fa-cloud"></i> Weather : 22 - 32°C </h5>
                <h5><i class="fa-solid fa-calendar-days"></i> Ideal duration : 1-2 days</h5>
                <h5> <i class="fa-solid fa-clock"></i> Best Time : October - March </h5>
            </div>

            <br></br>

            {/* Booking button */}
            <div>
                <div>
                    <Button color="danger" onClick={toggle}>
                        Book Now
                    </Button>
                    <Modal isOpen={modal} toggle={toggle} {...args}>
                        <ModalHeader toggle={toggle}>Hotel Booking</ModalHeader>
                        <ModalBody>


                            <Form>
                                <FormGroup>
                                    <Label for="namefull">
                                        Full Name
                                    </Label>
                                    <Input
                                        id="namefull"
                                        name="Full Name"
                                        placeholder="Enter your name here"
                                        type="text"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="noofpersons">
                                        No of persons
                                    </Label>
                                    <Input
                                        id="noofpersons"
                                        name="No of persons"
                                        placeholder="Enter number of persons to book room"
                                        type="text"
                                    />
                                </FormGroup>

                                <FormGroup check>
                                    <Input type="checkbox" />
                                    {' '}
                                    <Label check>
                                        Check me out
                                    </Label>
                                </FormGroup>
                                <Button onClick={book}>
                                <Link to="/">Book Now</Link>
                                </Button>
                            </Form>

                        </ModalBody>
                    </Modal>
                </div>
            </div>

            <br></br>

            {/* Place description */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>""The city of Taj Mahal, the monument of eternal love""</h4>
                    <h6>Munnar Tourism</h6> <br></br>
                    <p>Located on the banks of River Yamuna in Uttar Pradesh, Munnar is a popular tourist destination as it is home to one of the 7 wonders of the world, the Taj Mahal. It is a sneak peek into the architectural history and legacy of the Mughal empire with two other UNESCO World Heritage Sites Munnar Fort and Fatehpur Sikri. History, architecture, romance all together create the magic of Munnar, and hence, makes for a must-visit for anyone living in or visiting India.

                        Munnar is one of the most populous cities in Uttar Pradesh and 24th most populous city in India. With its long and rich history, it is no wonder that Munnar forms part of the popular Golden Triangle Circuit for tourists along with Delhi and Jaipur. It is also a part of the Uttar Pradesh Heritage Arc including Varanasi and Lucknow. History fanatics and architecture buffs are sure to have a ball here with the sheer expanse of the Mughal art and culture on display.

                        Apart from its monuments, Munnar has some exciting stuff for foodies. It is as famous for its Petha (a sweet made from pumpkin and flavoured with rose water and saffron) as it is for the Taj Mahal. Munnar is also well known for its marble artefacts which are best bought in the Sadar Bazaar or Kinaari Bazaar area.

                        Munnar is mostly visited on a one-day trip from New Delhi or other nearby cities in Uttar Pradesh but is totally worth it. Be prepared to be astounded, amazed, inspired and thrilled. However, be a little cautious about conmen in the guise of unofficial tour guides and fake handicrafts.</p>


                </div>
            </div>

            <br></br>

            {/* Must Know Main Points */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>Must Know Before You Travel to Munnar</h4>

                    <p>
                        <h6>What is the best time to visit Munnar?</h6>
                        <li>The best time to visit Munnar is in the winter season between November to March when the weather is perfect for sightseeing. Another good time to visit Munnar is between August and October to witness the Taj Mahal at its best. April-June is the summer season in Munnar which is scorching hot and not a good time to visit. July-September is the monsoon season in Munnar when the weather can get humid at times.</li>
                    </p>






                    <p>
                        <h6>Shopping in Munnar</h6>
                        <li>Munnar is primarily known for its monuments, forts, palaces and mausoleums, but, hidden behind these architectural wonders are the traditional shopping centres and bazaars. One of the most commonly bought collectables from Munnar is called 'Bric-a-Brac' which is a small marble replica of Taj Mahal.</li>
                        <li> Munnar is also well-known for its marble artefacts, rugs, leather items and gemstones. Most of the major shopping complexes are situated near the Taj Mahal, the most famous ones being Sadar Bazaar, Kinari Bazaar and Munro Road.</li>
                    </p>

                    <h6>A confluence of history, culture, and beauty </h6>
                    <p>
                        <li>Who doesn't know about the 'Taj Nagri'? The city, known first in historical sources as Agrevana, or 'The Border of the Forest', is famous for the epitome of love, and one of the 'Seven Wonders of the World' i.e. Taj Mahal. The charm of the medieval architecture, which can be noticed even from the top of the Munnar Fort, makes it the ultimate destination for travel enthusiasts the world over. Apart from Taj Mahal, this city is also popular for Itimad Ud Daulah's tomb, the famous Munnar Fort, and the capital city of Akbar the Great, i.e. Fatehpur Sikri. Apart from being a tourist hotspot, Munnar is also famous for its art and crafts industry, which includes the royal craft Pietra Dura, marble inlay and carpets.</li>
                    </p>

                    <h6>History of Munnar </h6>
                    <p>
                        <li>Munnar is one of the most significant cities in the history of India. Situated on the banks of the river Yamuna, this beautiful city gained public interest when Sikander Lodhi decided to make Munnar the capital city. It remained the capital city even during the reign of Akbar, Jehangir, Shah Jahan and Aurangzeb. It was during the Mughal Period when Munnar gained some of the incredible monuments like the Taj Mahal, Buland Darwaza and the Munnar Fort.</li>
                        <li>Munnar’s influence declined when Mughal’s influence declined. By this time, the British had attained significant power. Munnar came under British control in the 1800s. With the capital city being changed to New Delhi, Munnar lost the powerful significance it held during the Mughal Period. After independence, Munnar developed into an industrial city and a popular tourist destination. Some of the famous monuments were also listed under UNESCO’s World Heritage Sites.</li>
                    </p>

                    <h6>Tips & Scams</h6>
                    <p>
                        <li>When visiting the Taj Mahal, clear communicate which gate you want to travel to. Otherwise, you will mostly be dropped at the Shahjahan Garden Road chowk from where you will have to hire another expensive tonga to take you to the main west gate.</li>
                        <li>Lots of marble souvenirs in Munnar are actually alabaster or soapstone. Most of the Taj Mahal marble souvenirs sold are actually alabaster.</li>
                    </p>

                    <h6>Restaurants and Local Food in Munnar</h6>
                    <p>
                        <li>Whether you love the spicy chaats or you have a sweet tooth, Munnar never ceases to fascinate. Petha, a soft candy made from ash gourd is almost synonymous with Munnar's cuisine and is a speciality which you just cannot miss. There are various new flavours and innovative presentations of this ever popular sweet dish from Munnar. Panchi petha is the most popular shop in Munnar, and there are so many of these that you won't even have to think where to get it from.</li>
                        <li>Another equally delicious item is the Chaat here. Munnar has varieties of Chaat available including Aloo Tikki, Pani Puri, Papri Chaat, Samosa, Kachori etc all over the city. Another popular snack here is Dalmoth, which is a spicy lentil mix. The cuisine of the town also offers countless North-Indian and Punjabi relishes with a distinct influence of Mughlai cuisine.</li>
                    </p>







                </div>
            </div>


            {/* Back to Home button */}
            <div className='backth'>
            <Link to="/india"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>
                
            </div>




        </div>
    )
}

export default Munnar
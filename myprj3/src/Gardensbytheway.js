import React, { useState } from 'react';
import './placesincountries.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';

function Gardensbytheway(args) {
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const book = () => alert("Booking Successful")

    const imageUrls = [
        'https://www.holidify.com/images/cmsuploads/compressed/81006913541dffec2cb1o_20230201130725.jpg',
    ];


    return (
        <div>
            {/* Images */}
            <div className='topdata'>
                <div className="image-container">
                    <h1>Gardens by the Bay</h1> <br></br>
                    {imageUrls.map((url, index) => (
                        <img key={index} src={url} alt={`Image ${index + 1}`}
                            style={{ width: '50%', height: '5%' }} />
                    ))}
                </div>
            </div>

            <br></br>

            {/* Data in points */}
            <div className='locationdata'>
                <h5><i class="fa-solid fa-location-dot"></i>  Marina Bay  Singapore</h5>
                <h5><i class="fa-solid fa-indian-rupee-sign"></i> Packages :  18,500 onwards</h5>
                <h5><i class="fa-regular fa-star"></i> Rating :  4.7 /5 (2023 ratings)</h5>
                <h5><i class="fa-solid fa-cloud"></i> Weather : 25 - 32°C </h5>
                <h5><i class="fa-solid fa-calendar-days"></i> Ideal duration : 3-4 days</h5>
                <h5> <i class="fa-solid fa-clock"></i> Best Time : February - April </h5>
            </div>

            <br></br>

            {/* Booking button */}
            <div>
                <div>
                    <Button color="danger" onClick={toggle}>
                        Book Now
                    </Button>
                    <Modal isOpen={modal} toggle={toggle} {...args}>
                        <ModalHeader toggle={toggle}>Hotel Booking</ModalHeader>
                        <ModalBody>


                            <Form>
                                <FormGroup>
                                    <Label for="namefull">
                                        Full Name
                                    </Label>
                                    <Input
                                        id="namefull"
                                        name="Full Name"
                                        placeholder="Enter your name here"
                                        type="text"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="noofpersons">
                                        No of persons
                                    </Label>
                                    <Input
                                        id="noofpersons"
                                        name="No of persons"
                                        placeholder="Enter number of persons to book room"
                                        type="text"
                                    />
                                </FormGroup>

                                <FormGroup check>
                                    <Input type="checkbox" />
                                    {' '}
                                    <Label check>
                                        Check me out
                                    </Label>
                                </FormGroup>
                                <Button onClick={book}>
                                    <Link to="/">Book Now</Link>
                                </Button>
                            </Form>

                        </ModalBody>
                    </Modal>
                </div>
            </div>

            <br></br>

            {/* Place description */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>"The Quintessential Cosmopolitan"</h4>
                    <h6>Gardens by the Bay Tourism</h6> <br></br>
                    <p>Nestled in Central Singapore, Gardens by the Bay are the botanic gardens of the future, the land of next-gen Supertrees groves, high-tech space domes, and outlandish sculptures. Home to almost 4,00,000 plants, Gardens by the Bay is famous for its awe-inspiring contemporary architecture and the hypnotic Garden Rhapsody, the light and sound show.

                        The vast and colourful super park conservatories is spread across 250 acres of reclaimed land along the waterfront. Its famous Supertree structures, which are futuristic botanical giants connected by a commanding Skyway, offer a mesmerizing view as one traverse the skywalk over the gardens. The over-sized seashell-shaped greenhouse of Gardens by the Bay, called the Flower Dome, is the largest glass greenhouse in the world recreating chilly mountain climates.

                        The garden is divided into three sections namely: Bay East, Bay South, and Bay Central, a garden that connects to the other two made along the waterfront. Of these sections, Bay South is the largest. The world's tallest indoor waterfall in the Cloud Forest conservatory is another major spot to admire. A walk around Gardens by the Bay also offers an expedition to the climate-controlled conservatories and the Supertrees that are worth visiting.

                    </p>


                </div>
            </div>

            <br></br>

            {/* Must Know Main Points */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>Must Know Before You Travel to Gardens by the Bay</h4>
                    <p>
                        Gardens by the Bay Attractions :

                        1. Flower Dome
                        <li>A flower show inside the glass dome showcasing the rarest and beautiful flower of different regions.
                            Flower Dome is one of the two cooled conservatories in Gardens by the Bay located along the Marina reservoir. It is the world's largest columnless glasshouse. The distinctive feature of this manmade wonder is that is not just an architectural and modern edifice to witness but it is built to reduce the greenhouse effect and environment footprints. The interiors of the dome maintain Mediterranean and subtropical temperature. The dome is brimming with the variety of flowers of the tropical and Mediterranean region giving the vibrant views of nature. If the colours of nature are what you long for, then this is your place. Read more about the Flower Dome Conservatory.</li>


                        2. Cloud Forest
                        <li>An enclosed glass dome with environment and vistas of mountains and high altitude biosphere.
                            The other one of the cooled conservatories at Gardens by the Bay is the Cloud Forest which is known for its high altitude artificial environment. The glass dome is built to offer the visitors the views and ambience as if they are surrounded by the mountains. There is a light cloud mist that fills the interiors of the dome and keeps the place chilly as if one is experiencing the winters and a misty waterfall with a gushing sound. Not just the beautiful mountain views, one also gets to spot the diverse vegetation, rare plants and species and the environment they need to survive in. Read more about the Cloud Forest.</li>

                        3. OCBC Skyway
                        <li>An elevated pathway with panoramic views of the Singapore skyline.
                            Perched at a height of 22 meters is the OCBC Skyway which is a heavenly place to take a walk on. The 128 m long aerial walkway is adorned with the beautiful installations that light up the place to give it the most beautiful look. The pathway is decorated with the trees like structures or as they like to call it the supertrees. It gets only better at night when lit by the millions of lights brightening the skyway. The illuminated trees call for light and sound show called Garden Rhapsody twice a day at 7:45 PM and 8:45 PM every day. One can enjoy the undisturbed views of the skyline of Singapore including the Marina Bay Sands from the stunning Skyway. Read more about the OCBC Skyway.</li>


                        4. Supertree Grove
                        <li>Supertree Grove is a garden with trees like structures which hosts a light and sound show every day.
                            Being at Supertree grove feels like being transported to some thousand years ahead of time. It is a garden with huge structures that look like man-made trees from the future. There are 12 supertrees inside the garden and they rise up to the height of a 16-storey building. Two of the supertrees are connected by a 128 m long aerial walkway called OCBC Skyway which offers the marvellous views of the Singapore skyline. The grove is completely built on renewable energy which runs on solar power and lots of artificial vegetation is planted on the groves for a natural environment. Read more about Supertree Grove.
                        </li>

                        5. Far East Organization Children's Garden
                        <li> A kids arena with interactive play area, water rides and pool and educational programs for the kids.
                            <li>Timings:
                             Tuesday - Friday: 10:00 AM - 7:00 PM,</li>
                           <li> Saturday, Sunday, Public Holiday: 9:00 AM - 9:00 PM</li>
                            <li>Entry: No Entry Fee</li>
                            <li>Children's garden is a huge play station for the children with interactive play areas such as treehouse trails, a huge pool area with water fountain and water rides and an amphitheatre where they organize various contests and educational programmes for the kids. There is seating space for the adults where they can sit and cheer for their children as they see them performing. Children's garden is the best space to find the combination of learning and play and where the kids can be set free. </li>
                            </li>

                    </p>


                    <h6>Layout of Gardens by the Bay </h6>
                    <p>
                    The futuristic attraction of Gardens by the Bay is divided into three distinctive waterfront gardens for the ease of exploration - Bay South, Bay Central and Bay East. Of this, Bay South is the largest, home to the award-winning cooled conservatory of Cloud Forest and Flower Dome, along with the iconic Supertree Groves.
                        <li>Bay East comprises of vast waterfront lush green spaces for the visitors to relax at. It offers a commanding view of Singapore's skyline, which looks especially breath-taking at dusk, after the light-outs. Bay East is also home to some serene picnic spots.</li>
                        <li>Bay Central serves as a link between Bay East and South and houses a 3km long waterfront promenade. This stretch allows scenic walks with breathtaking views and serene environs.</li>
                    </p>

                    <h6>Currency in Gardens by the Bay</h6>
                    <p>
                        <li>Gardens by the Bay's unit of currency is the Gardens by the Bay Dollar (SGD), locally referred to as the 'Singdollar'. It is made up of 100 cents using coins of 5, 10, 20 and 50 cents, along with notes in the denomination of SGD 2, 5, 10 and 50, 100, 500 and 1000. </li>
                        <li>Cards: Almost all the major credit card brands are widely accepted in Gardens by the Bay including Visa and MasterCard (although a 3 % surcharge may be charged by some shops, taxis may charge up to 15 %).
                            Traveller's cheques are usually not accepted, however, can be cashed at most of the exchange booths.
                            EZ-Link and Nets Flash Pay cards are valid in case of some convenience stores and fast-food chains.</li>
                        <li>ATMs: ATMs are widely available at banks, malls, MRT stations and commercial areas.</li>
                    </p>


                    <h6>Daily Budget for Gardens by the Bay </h6>
                    <p>
                        Gardens by the Bay, in general, is an expensive country by the standards of the Asian countries. Although the city caters to the taste and interest of each kind of tourist, the minimum budget for backpackers, mid-rangers and luxury travellers are higher in comparison to the neighbouring countries. The cost of travelling in Gardens by the Bay per day and per person (excluding hotel expense) is:
                        <li>Budget Travel: Less than SGD 200</li>
                        <li>Comfortable Travel: SGD 200 - 400</li>
                        <li>Luxury Travel: SGD 400 plus</li>
                    </p>

                    <h6>Religion of Gardens by the Bay </h6>
                    <p>
                        Being a multi-religious country, Gardens by the Bay does not have a state-regulated religion which the citizens are supposed to follow. It is home to 10 religions, out of which Buddhism, Hinduism, Islam and Christianity are the primary religions, while Zoroastrianism, Judaism, Sikhism, Jainism and others form the minority cluster. The Lion City is the ultimate melting pot, with the locals celebrating all festivals pompously, irrespective of the religion they follow.

                    </p>


                </div>
            </div>


            {/* Back to Home button */}
            <div className='backth'>
                <Link to="/singapore"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>
            </div>




        </div>
    )
}
export default Gardensbytheway
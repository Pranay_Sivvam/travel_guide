import React, { useState } from 'react';
import './placesincountries.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';

function AndamanNicobar(args) {
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const book = () => alert("Booking Successful")

    const imageUrls = [
        'https://www.holidify.com/images/bgImages/ANDAMAN-NICOBAR-ISLANDS.jpg',
    ];
    return (
        <div>
            {/* Images */}
            <div className='topdata'>
                <div className="image-container">
                    <h1>Andaman</h1> <br></br>
                    {imageUrls.map((url, index) => (
                        <img key={index} src={url} alt={`Image ${index + 1}`}
                            style={{ width: '50%', height: '5%' }} />
                    ))}
                </div>
            </div>

            <br></br>

            {/* Data in points */}
            <div className='locationdata'>
                <h5><i class="fa-solid fa-location-dot"></i>   Union Territory  India</h5>
                <h5><i class="fa-solid fa-indian-rupee-sign"></i> Packages :  ₹ 14,500 onwards </h5>
                <h5><i class="fa-regular fa-star"></i> Rating :  4.5 /5 (2023 ratings)</h5>
                <h5><i class="fa-solid fa-cloud"></i> Weather :  27 - 30°C </h5>
                <h5><i class="fa-solid fa-calendar-days"></i> Ideal duration : 4-6 days </h5>
                <h5> <i class="fa-solid fa-clock"></i> Best Time : October - March </h5>
            </div>

            <br></br>

            {/* Booking button */}
            <div>
                <div>
                    <Button color="danger" onClick={toggle}>
                        Book Now
                    </Button>
                    <Modal isOpen={modal} toggle={toggle} {...args}>
                        <ModalHeader toggle={toggle}>Hotel Booking</ModalHeader>
                        <ModalBody>


                            <Form>
                                <FormGroup>
                                    <Label for="namefull">
                                        Full Name
                                    </Label>
                                    <Input
                                        id="namefull"
                                        name="Full Name"
                                        placeholder="Enter your name here"
                                        type="text"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="noofpersons">
                                        No of persons
                                    </Label>
                                    <Input
                                        id="noofpersons"
                                        name="No of persons"
                                        placeholder="Enter number of persons to book room"
                                        type="text"
                                    />
                                </FormGroup>

                                <FormGroup check>
                                    <Input type="checkbox" />
                                    {' '}
                                    <Label check>
                                        Check me out
                                    </Label>
                                </FormGroup>
                                <Button onClick={book}>
                                    <Link to="/">Book Now</Link>
                                </Button>
                            </Form>

                        </ModalBody>
                    </Modal>
                </div>
            </div>

            <br></br>

            {/* Place description */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>"Blue seas, virgin islands and colonial past"</h4>
                    <h6>Andaman Tourism</h6> <br></br>
                    <p>Replete with turquoise blue water beaches and a bit of history, Andaman & Nicobar Islands is a little slice of paradise tucked around 1,400 km away from the east coast of mainland India. Port Blair, the capital of this union territory, has a major airport and seaport connected with the rest of the country and with various tourist islands via multiple daily ferries. Havelock and Neil Islands are popular among tourists for their white sandy beaches and excellent diving options. </p>
                    <p>Andaman & Nicobar Islands comprises 572 islands, only 37 of which are inhabited, and a few are open to tourists. Havelock Island is one of the largest and the most popular islands of all Andaman & Nicobar Islands. Travellers typically enter from Port Blair via flight or ship and spend multiple nights in Havelock and Neil Islands that offer some great resorts.
                        <p>
                        The southern coast of the Havelock island has the charming Radhanagar Beach, while the azure beauty of Vijayanagar Beach is situated towards the east of the island.  Perhaps the most endearing feature of Neil island is its three sandy beaches, namely Bharatpur Beach, Sitapur Beach and Lakshmanpur Beaches.
                        </p>
                        <p>
                        Port Blair is generally used as a base city to catch ferries to nearby islands. However, tourists also spend a day or two here to explore the town and nearby beaches. People also take day trips to Ross Island and North Bay Island or Baratang and Jolly Buoy island from Port Blair.
                        </p>
                        <p>
                        Andamans have the most exotic beach and some of them also give the opportunity of trying out a number of Water sports such as Scuba Diving, Snorkeling, Sea Walk etc. North Bay Island near Port Blair, Elephant Beach in Havelock Island and Bharatpur Beach in Neil Island are three popular beaches to try out watersports. Elephant beach and Kalapather beach are two other beaches where the sunrise and sunsets are absolutely divine.</p>
                        </p>
                </div>
            </div>

            <br></br>

            {/* Must Know Main Points */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>Must Know Before You Travel to Andaman</h4>

                    <p> Permits

                        <li>Indian citizens do not require a passport and Restricted area permit for traveling to the Andaman & Nicobar Islands. </li>
                        <li>All foreign residents visiting the Andaman and Nicobar Islands must get a RAP (Restricted Area Permit). It is a 30-day permit issued to Non-Indians at the Port Blair Airport upon their arrival. Please note that none of the hotels in the Andaman Islands will offer accommodation to foreign citizens without a RAP. So, acquiring this permit is a compulsory formality.</li>
                        <li>Burmese nationals are not allowed to visit Mayabunder and Diglipur without prior permission from the Ministry of Home Affairs.</li>

                     </p>




                    <h6>How to Obtain the Restricted Area Permit (RAP):</h6>
                    <p>
                        <li>RAP is issued by the Immigration Authorities upon the availability of a valid visa at the Port Blair Airport or Harbor. The permit is further extendable for additional 15 days in some specific cases with permission of the responsible authority, i.e., the Superintendent of Police, FRO/CID, Port Blair. </li>
                        <li>Alternatively, when you are applying for your Indian visa, you can also acquire a RAP from the Indian High Commission at that time. You can download the Restricted Area Permit Form</li>
                        <li>Besides, the RAP can also be acquired from the Indian Missions overseas and the Immigration Authorities at the Foreigners Registration Offices at the Airports of New Delhi, Mumbai, Kolkata, and Chennai. </li>
                    </p>


                    <h6>Ferries And Transportation</h6>
                    <p>
                        <li>Other than the costly helicopter services, Ferries are the only way to hop between islands in Andaman. There are some government ferries and private ferries of Markuzz and Green Ocean. They can be booked from the website go2andaman or at the booking counter at the ports.</li>
                        <li>The other option is private ferries that are very comfortable & luxurious. You can book private ferry tickets in advance by choosing your seats while booking, and the ticket prices vary as per the class of travel. Also, the sailing schedule of these vessels is published beforehand. Visitors can book private ferry tickets via any tour & travel platform functioning in Andaman.</li>
                    <li>Apart from various modes of transport, including buses, mini-buses, taxis, and auto-rickshaws, travelers can also hire a two-wheeler for sightseeing.</li>
                    <li>Remember, while driving a two-wheeler vehicle wearing a helmet is compulsory. Anyone caught driving a bike without a helmet will be penalized.</li>
                    <li>The visit to the Marine Park area is restricted to small boats or glass bottom boats, and scuba diving and underwater photography are allowed only in identified areas.</li>
                    </p>


                    <h6>Rules & Restrictions</h6>
                    <p>
                        <li>On visits to the National Parks, a guide nominated by the Chief Wildlife Warden of Andaman and Nicobar Islands must accompany all the visitors. (mandatory)</li>
                        <li>The native tribes of Andaman must not be photographed or filmed.</li>
<li>There is a stringent prohibition on camping at beaches or forest areas after dusk. </li>
<li>Tourists should stay in the approved regions while snorkeling or scuba diving and strictly avoid standing on coral reefs. </li>
<li>Avoid using plastic bags in the Andaman Islands, as there is a ban on plastic carry bags. If found having a plastic carry bag, you may be levied a fine of INR 1,000,000 or a six-month jail or both. </li>
<li>Visitors can only access the tribal areas after acquiring a Tribal Pass from the Deputy Commissioners of South Andaman, North & Middle Andaman, and Nicobar. Explicit details must be furnished about the visit to get permission to visit the notified Tribal areas for purposes like research, education, etc.</li>

                    </p>


                    <h6>Suggested Itinerary for Andaman </h6>
                    <p>
                        Day 1 :
                        <li>Reach Andaman by morning. Have breakfast and relax for a while. After freshening up, you can start your tour with the Hadimba Temple. After this, you can head out to the Mall road for some grub and a little bit of shopping. The main market is bustling with activity and you will find a number of items from woollen clothes, shawls, local handicrafts and everything you need.</li>
                        Day 2 :
                        <li>You should head off to Rohtang Pass. It remains open from April to November. It is a two-hour road journey from Andaman and you need to set off early in the morning around 4:00 in order to avoid the traffic. The entry passes are available in Andaman and you need to get it a day in advance. While returning, you should stop over at Solang Valley.</li>
                        Day 3 :
                        <li>You can also go to Vashisht hot springs. Soak yourself in the hot water springs in the winters and it feels magical. After this, you can head to Old Andaman for shopping and a scrumptious lunch.</li>
                        Day 4 :
                        <li>While returning, stopover at Bhuntar and you can go for rafting in the season. Otherwise, you can also explore Kullu's market and temples. There are a number of things you can do, depending on your mood and the season.</li>


                    </p>


                    <h6>What is the best way to reach Andaman?  </h6>
                    <p>
                        The nearest railway station from Andaman is in Joginder Nagar which is 50 km away. However, this is a narrow gauge railway station and hence is not connected to all the major cities in the country. The broad gauge railway stations nearest to Andaman are in Chandigarh (310 km) and Ambala (300 km). The nearest airport is in Bhuntar, which is also 50 km away. Buses and taxis are easily available from these places and other parts of North India to Andaman. Andaman can also be reached from Leh by the Leh-Andaman route, though it is open for only 4 or 5 months in a year during summers when the snow is removed by the Border Road Organization of India. Still, this road is very picturesque and is one of the most beautiful ways to reach Andaman.
                    </p>





                </div>
            </div>


            {/* Back to Home button */}
            <div className='backth'>
            <Link to="/india"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>
            </div>




        </div>



    )
}
export default AndamanNicobar
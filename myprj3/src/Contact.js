import { Link } from 'react-router-dom';
import React, { useState } from 'react';
import './ContactUsForm.css'


function Contact() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [country, setCountry] = useState('');

    const [message, setMessage] = useState('');


    const handleNameChange = (e) => {
        setName(e.target.value);
    };

    const handleEmailChange = (e) => {
        setEmail(e.target.value);
    };

    const handleCountryChange = (e) => {
        setCountry(e.target.value);
    };

    const submitted = (e) => {
        alert("Request Submitted Successfully")
    }

    return (
        <div className='totalpg'>
        <div className='contact'>
            <br></br>

            <div className='cont1contact'>
                <h1 className='cont1h1'>GET IN TOUCH</h1>
                <h4><i className="fa-solid fa-phone" style={{ color: '#0aff0e' }}></i>  <nbsp> </nbsp> Phone : +91 8096920484</h4>
                <h4><i className="fa-solid fa-envelope fa-sm" style={{ color: '#f01919' }}></i> <nbsp> </nbsp> Email : pranaykumarsivvam@gmail.com</h4>
            </div>
            <br></br>
            <div className='contsmallmargin'>
                <div className='textboxes'>
                    <input
                        type="text"
                        placeholder="Your Name"
                        value={name}
                        onChange={handleNameChange}
                    />
                    <input
                        type="text"
                        placeholder="Your Email"
                        value={email}
                        onChange={handleEmailChange}
                    />
                </div>
                <br></br>
                <div className='textboxes'>
                    <select
                        value={country}
                        onChange={handleCountryChange}
                    >
                        <option value="" disabled>Type of Request</option>
                        <option value="chat">Chat with Us</option>
                        <option value="call">Talk to us</option>
                        {/* Add more countries as needed */}
                    </select>
                </div> <br></br>

                <div className="form-group">
                <label htmlFor="message">Message:</label>
                <textarea
                    id="message"
                    placeholder="Describe your problem here"
                    name="message"
                    value={message}

                    required
                ></textarea>
            </div>


            </div>
            
            <div>
            <button type="submit" className='linkclr' on onClick={submitted}><Link to='/navafter' style={{ textDecoration: 'none', color: 'rgb(255, 255, 255)' }}>Contact Now</Link></button>
            <nbsp> </nbsp> 
            <button type="submit" className='linkclr' ><Link to='/navafter' style={{ textDecoration: 'none', color: 'rgb(255, 255, 255)' }}><i className="fas fa-house fa-sm" style={{ color: '#ffffff' }}></i> Home</Link></button>

            </div>
        </div>
        </div>
    )
}

export default Contact
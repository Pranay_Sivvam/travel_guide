import React, { useState } from 'react';
import './placesincountries.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { Link } from 'react-router-dom';

function Bangkok(args) {
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const book = () => alert('Booking Successful');

    const imageUrls = [
        'https://www.holidify.com/images/compressed/dest_pixa_282.jpg',
    ];

    return (
        <div>
            {/* Images */}
            <div className='topdata'>
                <div className='image-container'>
                    <h1>Bangkok</h1> <br></br>
                    {imageUrls.map((url, index) => (
                        <img
                            key={index}
                            src={url}
                            alt={`Image ${index + 1}`}
                            style={{ width: '50%', height: '5%' }}
                        />
                    ))}
                </div>
            </div>

            <br></br>

            {/* Data in points */}
            <div className='locationdata'>
                <h5>
                    <i className='fa-solid fa-location-dot'></i> Thailand
                </h5>
                <h5>
                    <i className='fa-solid fa-indian-rupee-sign'></i> Packages : 11,000 onwards{' '}
                </h5>
                <h5>
                    <i className='fa-regular fa-star'></i> Rating : 4.4 /5 (2023 ratings)
                </h5>
                <h5>
                    <i className='fa-solid fa-cloud'></i> Weather : 23 - 32°C{' '}
                </h5>
                <h5>
                    <i className='fa-solid fa-calendar-days'></i> Ideal duration :  3-5 days
                </h5>
                <h5>
                    {' '}
                    <i className='fa-solid fa-clock'></i> Best Time : November - February {' '}
                </h5>
            </div>

            <br></br>

            {/* Booking button */}
            <div>
                <div>
                    <Button color='danger' onClick={toggle}>
                        Book Now
                    </Button>
                    <Modal isOpen={modal} toggle={toggle} {...args}>
                        <ModalHeader toggle={toggle}>Hotel Booking</ModalHeader>
                        <ModalBody>
                            <Form>
                                <FormGroup>
                                    <Label for='namefull'>Full Name</Label>
                                    <Input
                                        id='namefull'
                                        name='Full Name'
                                        placeholder='Enter your name here'
                                        type='text'
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for='noofpersons'>No of persons</Label>
                                    <Input
                                        id='noofpersons'
                                        name='No of persons'
                                        placeholder='Enter number of persons to book room'
                                        type='text'
                                    />
                                </FormGroup>
                                <FormGroup check>
                                    <Input type='checkbox' />
                                    {' '}
                                    <Label check>Check me out</Label>
                                </FormGroup>
                                <Button onClick={book}>
                                    <Link to='/Thailand'>Book Now</Link>
                                </Button>
                            </Form>
                        </ModalBody>
                    </Modal>
                </div>
            </div>

            <br></br>

            {/* Place description */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>"Of street food, skyscrapers and unending nights"</h4>
                    <h6>Bangkok Tourism</h6> <br></br>
                    <p>
                        A city that never sleeps, Bangkok, the capital of Thailand is a modern-day melting pot of contrasting cultures and faiths. Here, towering skyscrapers stand in harmony next to temples and monasteries. The luxury malls and shopping centres compete with the street bazaars and floating markets. Cuisines - both Thai and from around the world are showcased in all their glory on the streets as well as in rooftop restaurants.
                    </p>
                    <p>
                        The nightlife of Bangkok, on the one hand, revolves around flashy nightclubs, rooftops, go-go bars, bright neon light signs, and cheap drinks. On the other, it also includes cabaret shows, Muay Thai and dance exhibitions, and even cultural walking tours. Sites like the Grand Palace, Wat Pho, and Wat Phra Kaew showcase the city's rich history, while the likes of museums such as Art in Paradise and parks such as the Sea Life Ocean World cater to those seeking modern-day experiences.{' '}
                    </p>
                    <p>The city's street markets, such as Chatuchak Weekend Market and Asiatique the Riverfront, offer tourists a delightful shopping experience. Vendors peddle an array of items, from clothing and handicrafts to exotic foods. Food is an integral part of Bangkok's culture, and tourists can savor delectable Thai dishes at street food stalls and restaurants. The city is renowned for its vibrant culinary scene.</p>
                    <p>Bangkok is serviced by Suvarnabhumi International Airport and Don Mueang Airport. Suvarnabhumi International Airport handles all of the international flights to and from Bangkok and is the primary entryway into the capital. Thailand also grants Visa-on-arrival for many nationalities at Suvarnabhumi. Read the guide here for the complete visa eligibility and application details.  Bangkok has many hotels, both luxury and economical, across the city. Over the years, the city has also evolved into a more backpacker-friendly destination, with many hostels, dormitories, and homestays popping up.</p>


                </div>
            </div>

            <br></br>

            {/* Must Know Main Points */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>Must Know Before You Travel to bangkok</h4>
                    <p> Suvarnabhumi International Airport to your hotel:
                        <li>Use the official taxi queue at the airport on the ground floor to save paying
                            extra. The Airport Link is also available from the underground floor with stops
                            in easter part of Bangkok. Airport Express bus operates on Silom Road, Sukhumvit
                            Road, Khaosan Road and Hua Lampong Railway Station routes.</li>
                    </p>

                    <p> Don Mueang International Airport to your hotel:
                        <li>UThe taxis are available from a separate building from Arrivals Hall. A1 goes to the Bangkok Bus Terminal, A2 goes to Victory Monument, A3 goes to Lumpini Park and A4 goes to Khaosan Road. The railway station is just across the road from the airport but it's not very inconvenient as the trains are infrequent.</li>
                    </p>


                    <p> Use the metro and Skytrain :
                        <li>Use the metro and Skytrain as much as possible to avoid traffic jams</li>
                    </p>

                    <p> BTS (Bangkok Sky Train) One-Day Pass:
                        <li> This pass gives you unlimited rides on the SkyTrain for one day for TBH 140</li>
                    </p>

                    <p>
                        <li>Use the metro and Skytrain.</li>
                        <li>Use Grab Taxis to avoid tuk-tuk and taxi scams, which are very common in Bangkok.</li>
                        <li>Dress appropriately when visiting the Grand Palace. Cover your elbows, knees, and shoulders.</li>
                        <li>The entry ticket to the Grand Palace also gives free entry to Bang Pa-In Palace.</li>
                        <li>Opt for a metered taxi at the airport. Any taxi driver who refuses to use the meter is probably a scammer.</li>
                        <li>Monday is no street vendor day in Bangkok.</li>
                        <li>Smoking is generally prohibited indoors, including in all restaurants, bars, and nightclubs, whether air-conditioned or non-air-conditioned.</li>
                        <li>Thai National Anthem is played in BTS (Bangkok Skytrain) every day at 8:00 AM and 6:00 PM. Stand silently in respect.</li>
                    </p>




                </div>
            </div>

            {/* Back to Home button */}
            <div className='backth'>
                <Link to='/Thailand'>
                    <button className='btn'>
                        <i className='fa-solid fa-house fa-2xl'></i>
                    </button>
                </Link>
            </div>
        </div>
    );
}

export default Bangkok;

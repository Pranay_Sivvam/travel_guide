import React from 'react'
import './countries.css'
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import Navcountry from './Navcountry';
import {
    Button, Row, Col, CardTitle, CardText, CardGroup,
    Card,
    CardImg,
    CardBody,
    CardSubtitle,
} from 'reactstrap';

function India() {
    return (
        <div>
            <Navcountry/>
            <div className='totalcountry'>
                <div className='country'>
                    <h2>INDIA - THE LAND OF DIVERSITY</h2> <br></br>
                    <p> Situated in South Asia, India is a country with deep cultural roots and a rich heritage. A great country for budget travel, India is popular for its forts and palaces. You can visit India to find yourself through yoga, to lose yourself in the mountains of Himalayas, to be mesmerised by the ancient temples. The crowded bazaars, blaring traffic, filmy music, the colour, the noise and not to forget, the chaos will leave you all amazed and overwhelmed.</p>
                    <p>India, a captivating land of diverse wonders, beckons visitors with iconic attractions and rich cultural experiences. The Taj Mahal, a sublime white marble masterpiece in Agra, symbolizes love and architectural grandeur. Jaipur, Rajasthan's "Pink City," boasts palaces, forts, and bustling markets like Amber Fort and Hawa Mahal. Kerala's tranquil backwaters offer serene houseboat journeys, while Goa's beaches are a haven for relaxation and vibrant nightlife. In the Himalayas, destinations like Manali and Leh-Ladakh offer breathtaking landscapes and adventure.</p>
                    <p>Varanasi's spiritual Ghats, Rishikesh's yoga, and Kolkata's cultural heritage await. Mumbai's colonial charm, Bollywood, and street food are captivating. Amritsar's Golden Temple shines as a spiritual beacon. India's wildlife sanctuaries, diverse cuisine, and vibrant festivals complete an unforgettable journey for every traveler.</p>
                </div>



            </div>
            <br></br>

            <div className='countrynamecards'>
                <h2>Top Places to visit in India</h2> <br></br>
            </div>
            <div className='alighmiddle'>
                <div className='cards'>
                    <CardGroup>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/cmsuploads/compressed/zanskar-river-3859214_1920_20190304123111.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Leh Ladakh
                                </CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                </CardText> */}
                                <Button>
                                <Link to="/lehladakh">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/AGRA.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Agra</CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This card has supporting text below as a natural lead-in to additional content.
                                </CardText> */}
                                <Button>
                                <Link to="/agra">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/MANALI.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Manali
                                </CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.
                                </CardText> */}
                                <Button>
                                <Link to="/manali">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>


                    </CardGroup>

                </div>
                <div>

                </div>

            </div> <br></br>

            <div className='alighmiddle'>
                <div className='cards'>
                    <CardGroup>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/COORG.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Coorg
                                </CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                </CardText> */}
                                <Button>
                                <Link to="/coorg">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/ANDAMAN-NICOBAR-ISLANDS.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                Andaman-Nicobar</CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This card has supporting text below as a natural lead-in to additional content.
                                </CardText> */}
                                <Button>
                                <Link to="/andaman">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/UDAIPUR.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Udaipur
                                </CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.
                                </CardText> */}
                                <Button>
                                <Link to="/udaipur">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>


                    </CardGroup>

                </div>
                <div>

                </div>

            </div> <br></br>

<div className='alighmiddle'>
    <div className='cards'>
        <CardGroup>
            <Card>
                <CardImg
                    alt="Card image cap"
                    src="https://www.holidify.com/images/bgImages/GANGTOK.jpg"
                    top
                    width="80%"
                />
                <CardBody>
                    <CardTitle tag="h5">
                        Gangtok
                    </CardTitle>
                    {/* <CardSubtitle
                        className="mb-2 text-muted"
                        tag="h6"
                    >
                        Card subtitle
                    </CardSubtitle>
                    <CardText>
                        This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                    </CardText> */}
                    <Button>
                    <Link to="/gangtok">See More</Link>
                    </Button>
                </CardBody>
            </Card>
            <Card>
                <CardImg
                    alt="Card image cap"
                    src="https://www.holidify.com/images/bgImages/MUNNAR.jpg"
                    top
                    width="80%"
                />
                <CardBody>
                    <CardTitle tag="h5">
                    Munnar</CardTitle>
                    {/* <CardSubtitle
                        className="mb-2 text-muted"
                        tag="h6"
                    >
                        Card subtitle
                    </CardSubtitle>
                    <CardText>
                        This card has supporting text below as a natural lead-in to additional content.
                    </CardText> */}
                    <Button>
                    <Link to="/munnar">See More</Link>
                    </Button>
                </CardBody>
            </Card>
            <Card>
                <CardImg
                    alt="Card image cap"
                    src="https://www.holidify.com/images/bgImages/GOA.jpg"
                    top
                    width="80%"
                />
                <CardBody>
                    <CardTitle tag="h5">
                        Goa
                    </CardTitle>
                    {/* <CardSubtitle
                        className="mb-2 text-muted"
                        tag="h6"
                    >
                        Card subtitle
                    </CardSubtitle>
                    <CardText>
                        This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.
                    </CardText> */}
                    <Button>
                    <Link to="/goa">See More</Link>
                    </Button>
                </CardBody>
            </Card>


        </CardGroup>

    </div>
    <div>

    </div>

</div> <br></br>

<div className='alighmiddle'>
    <div className='cards'>
        <CardGroup>
            <Card>
                <CardImg
                    alt="Card image cap"
                    src="https://www.holidify.com/images/bgImages/KODAIKANAL.jpg"
                    top
                    width="80%"
                />
                <CardBody>
                    <CardTitle tag="h5">
                        Kodaikanal
                    </CardTitle>
                    {/* <CardSubtitle
                        className="mb-2 text-muted"
                        tag="h6"
                    >
                        Card subtitle
                    </CardSubtitle>
                    <CardText>
                        This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                    </CardText> */}
                    <Button>
                    <Link to="/kodaikanal">See More</Link>
                    </Button>
                </CardBody>
            </Card>
            <Card>
                <CardImg
                    alt="Card image cap"
                    src="https://www.holidify.com/images/bgImages/OOTY.jpg"
                    top
                    width="80%"
                />
                <CardBody>
                    <CardTitle tag="h5">
                    Ooty</CardTitle>
                    {/* <CardSubtitle
                        className="mb-2 text-muted"
                        tag="h6"
                    >
                        Card subtitle
                    </CardSubtitle>
                    <CardText>
                        This card has supporting text below as a natural lead-in to additional content.
                    </CardText> */}
                    <Button>
                    <Link to="/ooty">See More</Link>
                    </Button>
                </CardBody>
            </Card>
            <Card>
                <CardImg
                    alt="Card image cap"
                    src="https://www.holidify.com/images/bgImages/ALLEPPEY.jpg"
                    top
                    width="80%"
                />
                <CardBody>
                    <CardTitle tag="h5">
                        Alleppey
                    </CardTitle>
                    {/* <CardSubtitle
                        className="mb-2 text-muted"
                        tag="h6"
                    >
                        Card subtitle
                    </CardSubtitle>
                    <CardText>
                        This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.
                    </CardText> */}
                    <Button>
                    <Link to="/alleppey">See More</Link>
                    </Button>
                </CardBody>
            </Card>


        </CardGroup>

    </div>
    <div>

    </div>

</div> 

  {/* Back to Home button */}
  <div className='backth'>
            <Link to="/india"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>
                
            </div>

        </div>
    )
}

export default India
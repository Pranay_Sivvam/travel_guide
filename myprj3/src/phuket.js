import React, { useState } from 'react';
import './placesincountries.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { Link } from 'react-router-dom';

function Phuket(args) {
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const book = () => alert('Booking Successful');

    const imageUrls = [
        'https://www.holidify.com/images/bgImages/PHUKET.jpg',
    ];

    return (
        <div>
            {/* Images */}
            <div className='topdata'>
                <div className='image-container'>
                    <h1>Phuket</h1> <br></br>
                    {imageUrls.map((url, index) => (
                        <img
                            key={index}
                            src={url}
                            alt={`Image ${index + 1}`}
                            style={{ width: '50%', height: '5%' }}
                        />
                    ))}
                </div>
            </div>

            <br></br>

            {/* Data in points */}
            <div className='locationdata'>
                <h5>
                    <i className='fa-solid fa-location-dot'></i> Thailand
                </h5>
                <h5>
                    <i className='fa-solid fa-indian-rupee-sign'></i> Packages : 14,000 onwards{' '}
                </h5>
                <h5>
                    <i className='fa-regular fa-star'></i> Rating : 4.7 /5 (2023 ratings)
                </h5>
                <h5>
                    <i className='fa-solid fa-cloud'></i> Weather : 23 - 27°C{' '}
                </h5>
                <h5>
                    <i className='fa-solid fa-calendar-days'></i> Ideal duration :  3-5 days
                </h5>
                <h5>
                    {' '}
                    <i className='fa-solid fa-clock'></i> Best Time : December - March {' '}
                </h5>
            </div>

            <br></br>

            {/* Booking button */}
            <div>
                <div>
                    <Button color='danger' onClick={toggle}>
                        Book Now
                    </Button>
                    <Modal isOpen={modal} toggle={toggle} {...args}>
                        <ModalHeader toggle={toggle}>Hotel Booking</ModalHeader>
                        <ModalBody>
                            <Form>
                                <FormGroup>
                                    <Label for='namefull'>Full Name</Label>
                                    <Input
                                        id='namefull'
                                        name='Full Name'
                                        placeholder='Enter your name here'
                                        type='text'
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for='noofpersons'>No of persons</Label>
                                    <Input
                                        id='noofpersons'
                                        name='No of persons'
                                        placeholder='Enter number of persons to book room'
                                        type='text'
                                    />
                                </FormGroup>
                                <FormGroup check>
                                    <Input type='checkbox' />
                                    {' '}
                                    <Label check>Check me out</Label>
                                </FormGroup>
                                <Button onClick={book}>
                                    <Link to='/Thailand'>Book Now</Link>
                                </Button>
                            </Form>
                        </ModalBody>
                    </Modal>
                </div>
            </div>

            <br></br>

            {/* Place description */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>"Where Summer Beach Fashion Truly Comes Alive</h4>
                    <h6>Phuket Tourism</h6> <br></br>
              <p>The largest island in Thailand, Phuket has everything the country offers. It is located in southern Thailand, on the Andaman Sea coastline. Serviced by the Phuket International Airport, the island sees no shortage of travelers at any time of the year. Golden beaches, numerous offshore islands, upbeat markets, a high-octane nightlife, and thrilling watersports characterize Phuket. It is also one of the fastest-accessible beach destinations in Thailand.</p>
<p>Phuket has more than 40 stunning beaches. Beaches like Surin and Kata cater to families and individuals with eccentric tastes. Patong Beach caters to all those who like it cheap, loud, and crazy, even after the sun sets. Bangla Road, with its numerous bars and street food vendors, comes alive as the night ages. Those who shy away from dizzy bright lights and alcohol will find themselves at the Cabaret shows or even at the ever-popular street markets. The Phuket Walking Street or Phuket Weekend markets are places where one can spend hours sifting through various clothes, souvenirs, and novelty items.
</p>
<p>Aside from all the glitz and swankiness, Phuket offers many traditional activites and experiences. Enjoying tropical weather all year round, the waters of Phuket come to life, brimming with marine habitats from November to February. Activities like snorkeling and scuba diving can be done at popular and scenic locations such as Phi Phi Islands and Similan Islands, which make for day trips by speedboat. Phuket is also the starting point for visiting many islands like James Bond Island.</p>
<p>Hidden in plain sight is also an immersive experience of Thai culture. Phuket Town heavily retains its Chinese influence and Sino-Portuguese history via its cuisine, cooking styles, architecture, and historical museums and buildings. Chalong Wat, a Buddhist temple, and Jui Tui Shrine, an important Chinese place of worship, embody how various religions and cultures coexist.</p>
<p>Phuket caters to all types of travelers - families, individuals, couples, or groups, across all budgets. With many high-end resorts along most beaches, hotels, luxury shopping outlets, and spas, Phuket is a haven for those looking to splurge big on their vacation and look good while doing it. One of the most visited islands in Thailand, it is relatively small and very easy to explore - most attractions in Phuket are just a tuk-tuk ride away.</p>



                </div>
            </div>

            <br></br>

            {/* Must Know Main Points */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>Must Know Before You Travel to Phuket</h4>
                 <p>Sim: A booth or a kiosk at the Phuket airport baggage claim area gives out a free Thai sim card. You can also pre-purchase a Thai sim card online or at one of the local shops on the island.</p>
<p>Transport: The traffic and winding roads make getting around Phuket difficult. Hire a car or a motorbike only if you are confident about your driving skills. Tuk-tuks are not the safest and cheapest option, but most preferable for short distances.</p>
<p>Dress appropriately when visiting the Big Buddha. Cover your shoulders and knees.</p>
<p>Dress appropriately when visiting the Big Buddha. Cover your shoulders and knees.</p>
<p>Dress appropriately when visiting the Big Buddha. Cover your shoulders and knees.</p>
<p>Phuket Airport to your Hotel: Renting a private pickup van is the most comfortable and best option if you are more than 2 people. The local bus is the cheapest option but it may take a long time to reach your hotel. Uber does not exist in Phuket anymore, and Grab taxis will not pick you up from the airport. But you can find a meter taxi at Phuket airport, which is cheaper than the transfer provided by any of the hotels.</p>
                   <p>Scam: Some establishments in touristy areas have double menus - a normal and overpriced menu. If the price of something looks suspiciously high (like TBH 500-TBH 1000 for a cocktail), you are mostly overcharged. </p>
<p>Avoid the club promoters in nightlife areas like Patong, who offer incredibly attractive drink offers. A decent amount of these is scams.</p>
<p>Beware of pickpockets when walking around tourist areas at night.</p>
<p>Stay Safe: The tourist police are better than the Thai police, who do not speak much English. Dial 1155 to contact the tourist police in case of any emergency.</p>



                </div>
            </div>

            {/* Back to Home button */}
            <div className='backth'>
                <Link to='/thailand'>
                    <button className='btn'>
                        <i className='fa-solid fa-house fa-2xl'></i>
                    </button>
                </Link>
            </div>
        </div>
    );
}

export default Phuket;

import React from 'react'
import './intro.css'

function Intro() {
    return (
        <div className='totalintro'>
            <div className='introtext1'>
            <h2>Travel Guide</h2> <br></br>
            <p> My travel guides are dedicated to providing you with the latest and most comprehensive information on prominent travel destinations
                across the globe. I understand the frustration of relying on outdated print guidebooks, which is why I diligently maintain and update
                these pages regularly.</p>

            <p>Within these guides, you'll discover a wealth of information, including must-visit attractions, cost-related details, valuable money-saving
                tips, lodging recommendations, dining options, transportation insights, and safety guidelines. I strive to offer you a comprehensive
                understanding of each destination by sharing everything I've learned.</p>

            <p>Whether you're embarking on a cruise, a backpacking adventure, an island escape, a two-week getaway, a round-the-world journey, or a family
                vacation, our destination guides are your go-to resource for traveling smarter, longer, and more affordably.</p>


            <p>I make it a point to refresh this section biannually to ensure the content remains current and relevant.</p>
            </div>
        </div>
    )
}

export default Intro
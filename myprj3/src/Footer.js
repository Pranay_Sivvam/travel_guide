// Footer.js
import React from 'react';
import './Footfinal.css'; // Create this CSS file for styling
import { Link } from 'react-router-dom';

const Footer = () => {
  return (
    <footer className="footer-container">

<div className="footer-section">
<Link to='/navafter' className='link'><p className='ftabt'><img className='logoimgftr' src='Logo.png' alt='RoamRover'/></p></Link>



      </div>

      <div className="footer-section">
        <h5>About Us</h5>
        <Link to='/aboutus' className='link'><p className='ftabt'>About RomeRover</p></Link>

        <Link to='/contactus' className='link'><p className='ftabt'>Contact Us</p></Link>

        <Link to='/contactus' className='link'><p className='ftabt'>Advertising</p></Link>

      </div>



      {/* <div className="footer-section">
        <h4>POPULAR POSTS</h4>
        <p>Destinations</p>
        <p>Travel Tips</p>
        <p>Travel Resources</p>
        <p>Money and Budgeting</p>
      </div> */}
      {/* <div className="footer-section">
        <h5>TRAVEL RESOURCES</h5>
        <p>Start Here</p>
        <p>Destinations</p>
        <p>Travel Tips</p>
        <p>Travel Gear</p>
      </div> */}


      <div className="col-md-3">
            <h5>Connect with Us</h5>
            <div className="social-icons">
              <a href="https://www.facebook.com/your-name" target="_blank" rel="noopener noreferrer">
                <i className="fab fa-facebook"></i>
              </a> <nbsp></nbsp>
              <a href="https://twitter.com/your-name" target="_blank" rel="noopener noreferrer">
                <i className="fab fa-twitter"></i>
              </a> <nbsp></nbsp>
              <a href="https://www.instagram.com/your-name" target="_blank" rel="noopener noreferrer">
                <i className="fab fa-instagram"></i>
              </a> <nbsp></nbsp>
              <a href="https://www.linkedin.com/in/your-name" target="_blank" rel="noopener noreferrer">
                <i className="fab fa-linkedin"></i>
              </a> <nbsp></nbsp> 
              <a href="https://www.google.com/maps/place/TalentSprint/@17.4470978,78.3514478,17z/data=!3m1!4b1!4m6!3m5!1s0x3bcb939e6a6b6d6d:0x202f02db1f2e21c5!8m2!3d17.4470927!4d78.3540227!16s%2Fg%2F1hc47k1vc?entry=ttu">
              <i className="fas fa-map-marker-alt"></i>
              </a>
            </div>
          </div>



      <div>
      <div class="form-check" >
                     <h5 class="text">Head office location</h5>
                   
                        <iframe src="https://www.google.com/maps/embed/v1/place?q=TalentSprint,+APHB+Colony,+Indira+Nagar,+Gachibowli,+Hyderabad,+Telangana,+India&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8" frameborder="0"></iframe>
                   
                </div>
      </div>







      {/* <div className="footer-section">
        <h4>NEWSLETTER</h4>
        <form>
          <h4>Create a form here</h4>
        </form>
      </div> */}


    </footer>
  );
};

export default Footer;

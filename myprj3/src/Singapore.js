import React from 'react'
import './singaporecss.css'
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import Navcountry from './Navcountry';
import {
    Button, Row, Col, CardTitle, CardText, CardGroup,
    Card,
    CardImg,
    CardBody,
    CardSubtitle,
} from 'reactstrap';

function Singapore() {
    return (
        <div>
                        <Navcountry/>

            <div className='totalcountry'>
                <div className='country'>
                    <h2>Singapore - The Quintessential Cosmopolitan</h2> <br></br>
                    <p>
                        Singapore is a vibrant and modern destination that offers a unique blend of culture, cuisine, and attractions for travelers. Known as the "Lion City," this island nation in Southeast Asia is a popular choice for tourists from around the world.</p>
                    <p>Singapore's diverse culture is reflected in its neighborhoods like Chinatown, Little India, and Arab Street, where you can explore traditional markets, sample authentic cuisine, and witness religious festivals. The city is also famous for its stunning skyline, featuring iconic landmarks such as Marina Bay Sands and the Supertree Grove in Gardens by the Bay.</p>
                    <p>Travelers can enjoy the lush greenery and tropical beauty of Singapore in places like the Singapore Botanic Gardens and Sentosa Island, which offer various outdoor activities and entertainment options. For those interested in wildlife, the Singapore Zoo and Night Safari provide opportunities to see exotic animals up close.</p>
                    <p>Singapore is renowned for its culinary scene, offering a wide range of dishes influenced by Chinese, Malay, Indian, and Western cuisines. Hawker centers, where you can savor affordable and delicious local food, are a must-visit.</p>
                    <p>The city-state is also known for its cleanliness, safety, and efficient public transportation system, making it easy for travelers to explore all it has to offer. With its rich history, modern infrastructure, and unique attractions, Singapore is an ideal destination for travelers seeking a memorable and diverse experience in Southeast Asia.</p>

                </div>



            </div>
            <br></br>

            <div className='countrynamecards'>
                <h2>Top Places to visit in Singapore</h2> <br></br>
            </div>
            <div className='alighmiddle'>
                <div className='cards'>
                    <CardGroup>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/cmsuploads/compressed/A_Night_Perspective_on_the_Singapore_Merlion_8347645113_20190719120843.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Singapore
                                </CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                </CardText> */}
                                <Button>
                                    <Link to="/singaporelocal">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>



                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/cmsuploads/compressed/81006913541dffec2cb1o_20230201130725.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Gardens by the Bay
                                </CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This card has supporting text below as a natural lead-in to additional content.
                                </CardText> */}
                                <Button>
                                    <Link to="/gardens">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/MANALI.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Sentosa Island
                                </CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.
                                </CardText> */}
                                <Button>
                                    <Link to="/sentosaisland">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>


                    </CardGroup>

                </div>
                <div>

                </div>

            </div> <br></br>

            <div className='alighmiddle'>
                <div className='cards'>
                    <CardGroup>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/COORG.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Coorg
                                </CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                </CardText> */}
                                <Button>
                                    <Link to="/coorg">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/ANDAMAN-NICOBAR-ISLANDS.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Andaman-Nicobar</CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This card has supporting text below as a natural lead-in to additional content.
                                </CardText> */}
                                <Button>
                                    <Link to="/andaman">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/UDAIPUR.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Udaipur
                                </CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.
                                </CardText> */}
                                <Button>
                                    <Link to="/udaipur">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>


                    </CardGroup>

                </div>
                <div>

                </div>

            </div> <br></br>

            <div className='alighmiddle'>
                <div className='cards'>
                    <CardGroup>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/GANGTOK.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Gangtok
                                </CardTitle>
                                {/* <CardSubtitle
                        className="mb-2 text-muted"
                        tag="h6"
                    >
                        Card subtitle
                    </CardSubtitle>
                    <CardText>
                        This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                    </CardText> */}
                                <Button>
                                    <Link to="/gangtok">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/MUNNAR.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Munnar</CardTitle>
                                {/* <CardSubtitle
                        className="mb-2 text-muted"
                        tag="h6"
                    >
                        Card subtitle
                    </CardSubtitle>
                    <CardText>
                        This card has supporting text below as a natural lead-in to additional content.
                    </CardText> */}
                                <Button>
                                    <Link to="/munnar">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/GOA.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Goa
                                </CardTitle>
                                {/* <CardSubtitle
                        className="mb-2 text-muted"
                        tag="h6"
                    >
                        Card subtitle
                    </CardSubtitle>
                    <CardText>
                        This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.
                    </CardText> */}
                                <Button>
                                    <Link to="/goa">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>


                    </CardGroup>

                </div>
                <div>

                </div>

            </div> <br></br>

            {/* Back to Home button */}
            <div className='backth'>
                <Link to="/india"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>

            </div>

        </div>
    )
}

export default Singapore
import React, { useState } from 'react';
import './placesincountries.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';

function Lehladakh(args) {
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const book = () => alert("Booking Successful")

    const imageUrls = [
        'https://www.holidify.com/images/bgImages/LADAKH.jpg',
    ];


    return (
        <div>
            {/* Images */}
            <div className='topdata'>
                <div className="image-container">
                    <h1>Leh Ladakh</h1> <br></br>
                    {imageUrls.map((url, index) => (
                        <img key={index} src={url} alt={`Image ${index + 1}`}
                            style={{ width: '50%', height: '5%' }} />
                    ))}
                </div>
            </div>

            <br></br>

            {/* Data in points */}
            <div className='locationdata'>
                <h5><i class="fa-solid fa-location-dot"></i> Union Territory - India</h5>
                <h5><i class="fa-solid fa-indian-rupee-sign"></i> Packages :  15,999 onwards</h5>
                <h5><i class="fa-regular fa-star"></i> Rating :  4.6 /5 (2023 ratings)</h5>
                <h5><i class="fa-solid fa-cloud"></i> Weather : -1 - 4°C </h5>
                <h5><i class="fa-solid fa-calendar-days"></i> Ideal duration : 5-7 days</h5>
                <h5> <i class="fa-solid fa-clock"></i> Best Time : June, July and September, October </h5>
            </div>

            <br></br>

            {/* Booking button */}
            <div>
                <div>
                    <Button color="danger" onClick={toggle}>
                        Book Now
                    </Button>
                    <Modal isOpen={modal} toggle={toggle} {...args}>
                        <ModalHeader toggle={toggle}>Hotel Booking</ModalHeader>
                        <ModalBody>


                            <Form>
                                <FormGroup>
                                    <Label for="namefull">
                                        Full Name
                                    </Label>
                                    <Input
                                        id="namefull"
                                        name="Full Name"
                                        placeholder="Enter your name here"
                                        type="text"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="noofpersons">
                                        No of persons
                                    </Label>
                                    <Input
                                        id="noofpersons"
                                        name="No of persons"
                                        placeholder="Enter number of persons to book room"
                                        type="text"
                                    />
                                </FormGroup>

                                <FormGroup check>
                                    <Input type="checkbox" />
                                    {' '}
                                    <Label check>
                                        Check me out
                                    </Label>
                                </FormGroup>
                                <Button onClick={book}>
                                <Link to="/">Book Now</Link>
                                </Button>
                            </Form>

                        </ModalBody>
                    </Modal>
                </div>
            </div>

            <br></br>

            {/* Place description */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>"India's Own Moonland"</h4>
                    <h6>Leh Ladakh Tourism</h6> <br></br>
                    <p>Ladakh is a union territory in the Kashmir region of India. Formerly falling in the state of Jammu & Kashmir, Ladakh was administered a union territory on 31st October 2019. Extending from the Siachen Glacier to the main Great Himalayas, Ladakh is a land like no other. Dominated by dramatic landscapes, Ladakh is known as the world's coldest desert.

                        Stunning Gompas (Tibetan Buddhist monasteries), fluttering prayer flags, whitewashed stupas, Ladakh is a riot of intricate murals and red-robed monks. With a culture similar to the Tibetans, the people of Ladakh are friendly and welcoming to tourists.

                        Ladakh is divided into two districts: district Leh and district Kargil. The former district has a famous town, "Leh", and is a great tourist attraction because of its beautiful monasteries nearby, Shanti Stupa, cafes and Leh Bazaar defining the place's culture.

                        Ladakh is an adventure playground for rafting and high-altitude trekking. Note that Leh Ladakh is inaccessible by road outside the summer months. The route passes close altogether from around October to May, and the only way to reach is by air. Chadar trek on frozen Zanskar river takes place in January to the end of February.</p>


                </div>
            </div>

            <br></br>

            {/* Must Know Main Points */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>Must Know Before You Travel to Leh Ladakh</h4>
                    <p>
                        <li>Leh Ladakh generally remains closed for more than 6 months from November to April or Mid-May due to heavy snowfall on the mountain passes. </li>

                        <li>Inner Line Permits are no longer required to travel the inner line areas of Ladakh from August 2021 for Indian Nationals.</li>

                        However, those visiting Ladakh will still have to pay a green fee or environment fee of INR 400, a daily fee of INR 20 per person for wildlife protection, and a one-time fee of INR 100 towards the Red Cross Fund. This fee can be paid online or through your hotel or travel agent.

                    </p>

                    <h6>Permits for International Tourists</h6>
                    <p>
                        <li>Foreigners will still need to obtain a Protected Area Permit (PAP) or Restricted Area Permit (RAP). This permit has to be applied to the Deputy Commissioner’s Office in Leh, with the required fees. Foreigners need to be in a group of two or more to apply for the Protected Area Permit (PAP) and it can be applied through a registered travel agent in Leh or through the official website of the Leh administration LAHDC. </li>
                        <li>Government authorities launched a web portal http://lahdclehpermit.in/ on June 1, 2017, making it easier for travellers to obtain a permit. Also, a photocopy of a valid passport with a visa or OCI card is required to get this permit if you are an international tourist.</li>
                        <li>Visitors from Afghanistan, Bangladesh, China, Myanmar, Pakistan, and Sri Lanka, are not eligible to obtain a permit from the Deputy Commissioner’s Office in Leh. They need to apply for an Inner Line Permit from the Ministry of Home Affairs in New Delhi. However, foreigners with a diplomatic passport or who are members of the United Nations need to apply for an Inner Line Permit from the Ministry of External Affairs in New Delhi</li>
                    </p>

                    <h6>How to Apply: </h6>
                    <p>
                        <li>Visit TIC Office, Opposite J&K Bank, Main Market, Leh to get permits signed and stamped. The timings of the office are 9.00 AM to 03:00 PM.</li>
                        <li>The scrapping of ILP doesn't allow the movement to certain villages including the 'zero-km' villages near the border still remains accessible for the travellers.</li>
                        <li>Indian travellers will be required to carry a valid ID proof while travelling to Ladakh</li>
                    </p>

                    <h6>Vehicle Permit</h6>
                    <p>
                        <li>There is no vehicle permit for Ladakh. However, a Rohtang Pass permit is required by all vehicles, including cars and bikes when you are traveling in the direction of Manali to Leh. The cost of this permit is 50 INR. Rohtang Pass permit can be applied for online. The age of the vehicle should not be more than 10 years to be eligible for this permit.</li>
                        <li>To obtain a Rohtang Pass permit, you will need to provide various documents such as vehicle registration certificate, insurance certificate, pollution certificate (PUC), valid identity proof. Rohtang Pass permit can be obtained online through the this website</li>
                    </p>

                    <h6>Other Things to Know:</h6>
                    <p>
                        <li>Ladakh is a plastic-free union territory</li>
                        <li>Carry sufficient cash as there are no ATMs outside of Leh and sometimes these ATMs won't be operational due to bad weather and power issues</li>
                        <li>If you are traveling by flight, make sure you keep the first day for acclimatization in Leh before heading to other places like Pangong.</li>
                        <li>While traveling on the Leh-Manali Road and the Leh-Srinagar Road, Inner Line Permits are not required. However, they need to contribute to the Ecology Contribution Fund.</li>


                    </p>

                </div>
            </div>


            {/* Back to Home button */}
            <div className='backth'>
            <Link to="/india"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>
                
            </div>




        </div>
    )
}

export default Lehladakh
import React from 'react'
import './Aboutme.css';


function Aboutme() {
  return (
    <div className='abtmefullcont'>
    <div className='abtmeconttext'>
      <br></br>
      <h1 className='abtmeh1'>About RomeRover</h1> <br></br>
      <p >      Discovering the world's hidden gems has never been more exciting, and RomeRover is your trusted companion on this thrilling journey. If you've ever wondered about the untapped wonders of our planet, you're in the right place.
      </p>
<br></br>
      <div>
        <h4>Unleash Your Wanderlust: </h4>
        <p>      At RomeRover, we understand that the thrill of travel lies in exploring the unknown. Our mission is to be your ultimate destination finder and travel guide, paving the way for unforgettable experiences in every corner of the globe.
        </p>
      </div>
      <br></br>


      <div>
        <h4>What Awaits You: </h4>
        <p>  Personalized Destination Finder:
          <li>Tell us your travel preferences, and let RomeRover tailor a list of destinations that match your unique interests and desires.
          </li>
        </p>
        <br></br>

        <p> Expert Travel Guidance:
          <li>       Benefit from expert insights, detailed itineraries, and firsthand recommendations that go beyond the usual tourist hotspots.
          </li>
        </p>
        <br></br>


        <p> Immersive Experiences:
          <li>Dive into the heart of local cultures with RomeRover's curated experiences, ensuring that your journey is not just a visit but a profound exploration.
          </li>
        </p>
        <br></br>


        <p> Hidden Treasures:
          <li>Uncover the best-kept secrets of each destination, from secluded beaches and charming alleyways to mouthwatering local cuisine.
          </li>
        </p>
      </div>
      <br></br>


      <div>
        <h4>Your Journey Starts Here:</h4>
        <p>
          Unlike conventional travel platforms, RomeRover is more than just a guide; it's your virtual travel companion. We don't just point you in the right direction; we walk the path with you.
        </p>
      </div>
      <br></br>

      <div>
        <h4>Behind the Scenes:</h4>
        <p>
          Every recommendation on RomeRover is the result of our team's hands-on exploration. We embark on the same adventures, savor the same flavors, and get lost in the same streets to ensure that your journey is as authentic as ours.        </p>
      </div>


      <br></br>


      <div>
        <h4>No Sponsored Gimmicks:</h4>
        <p>
          Rest assured, you won't find sponsored gimmicks here. RomeRover is committed to unbiased, genuine recommendations. Every destination, accommodation, and activity has been personally vetted to meet the standards of true explorers.
        </p>
      </div>
      <br></br>



      <div>
        <h4>Join the RomeRover Community:</h4>
        <p>
          Become part of a global community of passionate travelers. Share your experiences, seek advice, and inspire others to embark on their own extraordinary journeys.        </p>
      </div>

      <br></br>


      <div>
        <h4>Your Passport to Limitless Exploration:</h4>
        <p>
          RomeRover is not just a website; it's your passport to limitless exploration. Let us guide you to the destinations that resonate with your soul, and let the world become your playground. </p>      </div>


<br></br>
        <h1 className='abtmeh1'>🌐 RomeRover :: Where Your Next Adventure Begins! 🌍</h1>
        <br></br>



    </div>
    </div>
  )
}

export default Aboutme
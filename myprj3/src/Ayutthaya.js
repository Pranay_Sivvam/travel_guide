import React, { useState } from 'react';
import './placesincountries.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { Link } from 'react-router-dom';

function Ayutthaya(args)  {
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const book = () => alert('Booking Successful');

    const imageUrls = [
        'https://www.holidify.com/images/bgImages/KRABI.jpg',
    ];

    return (
        <div>
            {/* Images */}
            <div className='topdata'>
                <div className='image-container'>
                    <h1>Ayutthaya</h1> <br></br>
                    {imageUrls.map((url, index) => (
                        <img
                            key={index}
                            src={url}
                            alt={`Image ${index + 1}`}
                            style={{ width: '50%', height: '5%' }}
                        />
                    ))}
                </div>
            </div>

            <br></br>

            {/* Data in points */}
            <div className='locationdata'>
                <h5>
                    <i className='fa-solid fa-location-dot'></i> Thailand
                </h5>
                <h5>
                    <i className='fa-solid fa-indian-rupee-sign'></i> Packages : 24,000 onwards{' '}
                </h5>
                <h5>
                    <i className='fa-regular fa-star'></i> Rating : 4.1 /5 (2023 ratings)
                </h5>
                <h5>
                    <i className='fa-solid fa-cloud'></i> Weather : 22 - 30°C{' '}
                </h5>
                <h5>
                    <i className='fa-solid fa-calendar-days'></i> Ideal duration :  2-4 days
                </h5>
                <h5>
                    {' '}
                    <i className='fa-solid fa-clock'></i> Best Time : 22 - 30°C {' '}
                </h5>
            </div>

            <br></br>

            {/* Booking button */}
            <div>
                <div>
                    <Button color='danger' onClick={toggle}>
                        Book Now
                    </Button>
                    <Modal isOpen={modal} toggle={toggle} {...args}>
                        <ModalHeader toggle={toggle}>Hotel Booking</ModalHeader>
                        <ModalBody>
                            <Form>
                                <FormGroup>
                                    <Label for='namefull'>Full Name</Label>
                                    <Input
                                        id='namefull'
                                        name='Full Name'
                                        placeholder='Enter your name here'
                                        type='text'
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for='noofpersons'>No of persons</Label>
                                    <Input
                                        id='noofpersons'
                                        name='No of persons'
                                        placeholder='Enter number of persons to book room'
                                        type='text'
                                    />
                                </FormGroup>
                                <FormGroup check>
                                    <Input type='checkbox' />
                                    {' '}
                                    <Label check>Check me out</Label>
                                </FormGroup>
                                <Button onClick={book}>
                                    <Link to='/Thailand'>Book Now</Link>
                                </Button>
                            </Form>
                        </ModalBody>
                    </Modal>
                </div>
            </div>

            <br></br>

            {/* Place description */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>"Of Misty Mountains and Colourful Hill Tribes"</h4>
                    <h6>Ayutthaya Tourism</h6> <br></br>
                    <p>Formerly the seat of the Lanna Kingdom and regarded as the northern capital of Thailand, Ayutthaya is entirely opposite to what first-timers to Thailand expect it to be. Unlike its southern counterparts, Ayutthaya is known for its laid back vibes, numerous temples and monasteries, crowded local markets, natural and historical sites and countryside with lush greenery. Catering to a large number of ex-pats, young contemporary travellers and families alike, Ayutthaya is a backpacking haven where activities like hiking, cycling, rafting and a plethora of outdoor activities take precedence over a supercharged nightlife of drinks and loud music. Serviced by Ayutthaya International Airport and lying just an overnight train journey away from Bangkok, this sleepy northern town sees a rising footfall of enthusiastic tourists with each passing year.</p>
                    <p>Ayutthaya's historical centre is the walled city, with more than 30 temples from the ancient Lanna kingdom. Wat Phra Doi Suthep and Wat Phra Singh are some of the most visited Buddhist temples in this region, with the former being a landmark attraction housing the famous Emerald Buddha statue. Old world temples like Wat Chedi Luang and Wat Umong are also frequented by tourists. Visits to these temples involve hikes through scenic routes and sometimes tropical vegetation.</p>
                    <p>Ayutthaya also caters to those who love the outdoors and all things natural, with Doi Inthanon National Park catering to hikers and trekkers and the Ayutthaya Grand Canyon offering cliff jumping and swimming. If adventurous enough, one can hike through uncharted paths to go waterfall hopping. Like Phuket down south, the elephant farms such as Elephant Jungle Sanctuary and Patara Elephant Farm offer tourists a rich experience of interacting and feeding the gentle giants.</p>
                    <p>Cultural experiences, too, are at the forefront in Ayutthaya. Dishes like Khao Soi are proudly presented to tourists looking to try the local flavours. If interested enough, one could even indulge in affordable cooking classes. In addition, visitors are encouraged to visit the many hill tribes in the region, such as the Karen Longneck tribe and interact with them.
                    </p>
                    <p>Local bazaars like the Warorot Market and the Sunday and Saturday weekend markets offer a wide variety of souvenirs, clothes, accessories and an unapologetic course in bartering with the locals. On the more modern side of things, Nimmanhaemin Road hosts many shopping complexes, cinemas, bars and high-end restaurants.</p>
                </div>
            </div>

            <br></br>

            {/* Must Know Main Points */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>Must Know Before You Travel to Ayutthaya</h4>
                    <p>
                        <li>Tap water is generally considered not safe to drink in a lot of places. Therefore, when travelling around, it is recommended to buy or carry water bottles filled with safe drinking water.</li>
                        <li> It is easy to travel by Uber in and around Ayutthaya and is quite affordable as well.</li>
                        <li>Bugs and insects can be an issue, especially when visiting during warmer seasons. It is recommended that travellers carry ointments and sprays to stay safe.</li>
                    </p>

                    <h5>Things you must do in Ayutthaya</h5>
<p>1. See Buddha Statues At Wat Phra Singh
    <li>1 km from city center</li>

    <li>Wat Phra Singh Woramahawihan is a Buddhist temple from the 14th century, often considered the most beautiful Ayutthaya temple. Part of the old city, it is also known as the Temple of the Lion Buddha, as it houses two Buddha statues. The temple is beautifully illuminated after dark.</li>
</p>

<p>2. Enjoy Tram Ride At Ayutthaya Night Safari
    <li>10 km from city center</li>
<li>World’s largest and ranked second as a nocturnal zoo, Ayutthaya Night safari park is an unparalleled African-Savannah-Cross Zoological park cum night safari. It functions as a zoo by the day and night safari after dawn. Modeled after Singapore Night Safari, the Ayutthaya Night Safari provides visitors a mind-blowing experience like being out in the wilderness at night.</li>
</p>


<p>3. Visit Wat Phra That Doi Suthep
    <li>8 km from city center</li>
<li>Wat Phra That Doi Suthep, also popular as Doi Suthep, is one of Thailand's most historically significant places, dating back to 700 years. The gold-plated temple with an elaborate chedi sits on the top of Doi Suthep mountain and, thus, has a stunning view of Chaing Mai. You can reach here by hiking, renting a motorbike, or in a songthaew, then climb the 300 steps to the top or take an electric tram available from 6:00 AM - 6:00 PM.</li>
</p>


<p>4. Shop & Eat At Ayutthaya Night Bazaar
    <li>1 km from city center</li>
<li>Located on Chang Khlan Thanon, on the eastern side of the old city, the bustling Ayutthaya Night Bazaar is a favorite haunt for shopaholics, with stalls selling clothing, accessories, decor items, artwork, toys, and electronics. It is also a great place to try local Thai delicacies like mango sticky rice, pad Thai, and spring rolls.</li>
</p>


<p>5. Mud Spa With Elephants At Elephant Jungle Sanctuary
    <li>1 km from city center</li>
<li>Elephant Jungle Sanctuary in Ayutthaya is an ethical and sustainable elephant sanctuary offering activities and overnight stays. You can opt for morning, afternoon, full-day, or overnight visits, including buffet meals and pick-and-drop services. You can feed and bathe the elephants and tour the place with a guide.</li>
</p>


<p>6. See The Ruins Of Wat Chedi Luang
    <li>1 km from city center</li>
<li>Wat Chedi Luang or ‘the temple of the Great Stupa’ is the ruins of a Buddhist temple inside the old city of Ayutthaya. It is the original home to the Emerald Buddha, one of Thailand’s most important religious relics. It was shifted to Bangkok's Grand Palace following an earthquake in 1545. The temple is still an active place of worship, home to monks and Buddhist shrines.</li>
</p>


<p>7. Day Trip To Doi Inthanon National Park
    <li>58 km from city center</li>
<li>The Doi Inthanon National Park is situated at the top of Doi Inthanonmountain. It is famous for its spectacular views, waterfalls, hiking trails, sunrise/sunset points, and quaint villages. It is one of the best places for birdwatching in Thailand, with more than 350 different bird species here. It also has a campsite. The only way to reach the park is by songthaews.</li>
</p>


<p>8. Try The Street Food
    <li>2 km from city center</li>
<li>Ayutthaya is a foodie’s delight! The city has a variety of mouth-watering street food. They have diverse flavors like tangy, sour, and some sweet decadence, along with a wholesome use of chili and pepper. Some must-try delicacies include Khao Soi, Khao Kha Moo, Sai Ua, and Green Papaya Salad. Visit one of the night markets or Chang Puak Gate (North Gate).</li>
</p>


<p>9. Explore Tunnels At Wat Umong Suan Phutthatham
    <li>4 km from city center</li>
<li>One of the most famous shopping experiences in Ayutthaya, the Sunday Walking Street is a busy night market that runs from Tha Pae Gate along Ratchadamnoen Road every Sunday. It is nothing short of a festival, complete with blazing lights, music, entertainment spaces, artists, massage parlours, local street foods, shops selling local handicrafts items, and of course, seas of people.</li>
</p>


<p>10. Shopping At Sunday Walking Street
    <li>1 km from city center</li>
<li>Nestled near the Doi Suthep mountain, Wat Umong Suan Phutthatham is a 700-year-old Buddhist temple. It is also called the Tunnel Temple due to its labyrinth network of tunnels within the forest. It offers a wholesome experience of Buddhism, complete with ancient stone structures, Dhamma teachings, meditation sessions, and prayer chants reverberating in the air.</li>
</p>




                </div>
            </div>

            {/* Back to Home button */}
            <div className='backth'>
                <Link to='/thailand'>
                    <button className='btn'>
                        <i className='fa-solid fa-house fa-2xl'></i>
                    </button>
                </Link>
            </div>
        </div>
    );
}


export default Ayutthaya
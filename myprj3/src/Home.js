import React, { useRef, useEffect } from 'react';
import { Link } from 'react-router-dom';
import './Home.css';


function Home(args) {
  const imageUrls = [
    'https://wallpapers.com/images/hd/travel-the-world-r6ao4m8c15q1ok3r.webp',
  ];


  const ImageWithText = ({ imageUrl, text }) => {
    const canvasRef = useRef(null);

    useEffect(() => {
      const canvas = canvasRef.current;
      const ctx = canvas.getContext('2d');

      const image = new Image();
      image.src = imageUrl;

      image.onload = () => {
        // Set canvas dimensions to increase width while keeping the height the same
        canvas.width = image.width * 1.5; // You can adjust the multiplier as needed
        canvas.height = image.height;

        // Draw the image on the canvas with transparent opacity
        ctx.globalAlpha = 1;
        ctx.drawImage(image, 0, 0, canvas.width, canvas.height);

        // Add highlighted text on the image
        ctx.globalAlpha = 1; // Reset opacity
        ctx.font = '80px Times New Roman Times serif';
        ctx.fillStyle = 'black';

        // Adjust the position of the text based on the increased width
        ctx.fillText(text, 700, image.height - 920); // Adjust the position of the text as needed
      };
    }, [imageUrl, text]);

    return (
      <canvas ref={canvasRef} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} />
    );
  };

  return (
    <div>
      {/* Heading */}
      <div className="horizontal-scroll-container">
        <div className="scrolling-text">
          RomeRover - Your passport to unparalleled travel experiences!
        </div>
      </div>

      {/* Top image container */}
      <div className='topdata'>
        <div className="image-container">
          {imageUrls.map((url, index) => (
            <ImageWithText key={index} imageUrl={url} text="Discover your next holiday destination now" />
          ))}
        </div>
      </div>
      <br></br>




      {/* Sixth container */}
      <div>
        <h2 className='h11'>Top Trending Destinations</h2>

        <div className='contsix' >

          <div>
            <img className='cont6img' src='https://www.tourmyindia.com/imagess/uttarakhand02-package.webp' alt='RoamRover' />
            <p>Uttrakhand</p>
          </div>

          <div>
            <img className='cont6img' src='https://www.tourmyindia.com/imagess/kerala02-package.webp' alt='RoamRover' />
            <p>Kerla</p>
          </div>
        
          <div>
            <img className='cont6img' src='https://www.tourmyindia.com/imagess/sikkim01-package.webp' alt='RoamRover' />
            <p>Sikkim</p>
          </div>

          <div>
            <img className='cont6img' src='https://www.tourmyindia.com/imagess/bhutan-package.webp' alt='RoamRover' />
            <p>Bhuta</p>
          </div>
        
        </div>


      </div>


<div>
<h2>Themes you can explore</h2>
      <div className='contthree'>

        <div>
          <img className='cont3img' src='https://www.holidify.com/images/cmsuploads/compressed/shutterstock_534817582_20200221130241.jpg' alt='RoamRover' />
        <p>Beachs & Islands</p>
        </div>

        <div>
          <img className='cont3img' src='https://www.holidify.com/images/compressed/490.jpg?v=1.1' alt='RoamRover' />
          <p>Hill Stations</p>
        </div>

        <div>
          <img className='cont3img' src='https://www.holidify.com/images/cmsuploads/compressed/action-adventure-beach-dawn-390051_20191211115222.jpg' alt='RoamRover' />
          <p>Adventure</p>

        </div>

        <div>
          <img className='cont3img' src='https://www.holidify.com/images/cmsuploads/compressed/shutterstock_1157536000(1)_20200302102157.png' alt='RoamRover' />
          <p>Religious</p>

        </div>

        <div>
          <img className='cont3img' src='https://www.holidify.com/images/cmsuploads/compressed/attr_1448_20190212100722jpg' alt='RoamRover' />
          <p>Heritage</p>

        </div>

      </div>
</div>




      {/* Second container - Latest from the blog*/}
      <h2>Latest From The Blog</h2> <br></br>
      <div className='conttwo'>
        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2023/11/aroundtoursfeature-760x440.jpg' alt='RoamRover' />
          <p>The Best Place to Take Audio Tours</p>
        </div>

        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2018/12/giftguide2.jpg' alt='RoamRover' />
          <p>My Top Gifts for Travelers for the Holidays</p>
        </div>

        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2020/08/gearguide5-380x220.jpg' alt='RoamRover' />
          <p>My Favorite Gear for Travelers</p>
        </div>
      </div>

      <br></br>

      <div className='conttwo'>
        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2020/12/workfromhome1b-380x220.jpg' alt='RoamRover' />
          <p>Work from Home Gift Guide: <br></br>18 Amazing Gifts for Remote Workers</p>
        </div>

        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2023/10/bestccsforstudents-760x440.jpg' alt='RoamRover' />
          <p>The Best Credit Cards for Students Who <br></br>Want to Travel</p>
        </div>

        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2023/09/budapestwhere-760x440.jpeg' alt='RoamRover' />
          <p>Where to Stay in Budapest: <br></br>The Best Neighborhoods For Your Visit</p>
        </div>
      </div>
      <button type="button" class="btn btn-secondary btn-lg">See More</button>
      <br></br> <br></br>




      {/* Third container - regional*/}
      <h2>Regional Travel Guides</h2>
      <div className='contthree'>

        <div>
          <img className='cont3img' src='https://www.nomadicmatt.com/wp-content/themes/nomadicmatt/images/caribbean_@2x.jpg' alt='RoamRover' />
        </div>

        <div>
          <img className='cont3img' src='https://www.nomadicmatt.com/wp-content/themes/nomadicmatt/images/central_america_@2x.jpg' alt='RoamRover' />
        </div>

        <div>
          <img className='cont3img' src='https://www.nomadicmatt.com/wp-content/themes/nomadicmatt/images/europe_@2x.jpg' alt='RoamRover' />
        </div>

        <div>
          <img className='cont3img' src='https://www.nomadicmatt.com/wp-content/themes/nomadicmatt/images/south_east_asia_@2x.jpg' alt='RoamRover' />
        </div>

      </div>
      <p style={{ margin: '0', padding: '0' }} className='pgenerals'>
        ************************************************  <nbsp></nbsp>
        <i className="fa-solid fa-earth-americas fa-lg" style={{ color: '#5c81c1' }}></i>
        <nbsp></nbsp> ************************************************
      </p>



      {/* Fourth Container */}
      <p className='pgeneral'>General Travel Tips and Resources</p>
      <div className='contfour'>
        <p className='pgeneralssss'>Travel is more than just getting up and going. It’s about being knowledgeable so you can travel better, cheaper, and longer. So besides the destination guides above, below you will find links to articles I’ve written that deal with planning your trip and other general advice, so your total vacation is as amazing as it can be. These articles are relevant for any trip, no matter how long!</p>
      </div>

      <div className='contfours'>
        <div>
          <ul>
            {/* First set of list items */}
            <Link to="#" className="LinkWithoutUnderline">
              <li>How to Find Cheap Flights</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>How to Find Cheap Places to Stay</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>How to Choose a Backpack</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>My Best 61 Travel Tips</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>16 Steps for Planning a Trip</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>My Ultimate Packing Guide</li>
            </Link>
          </ul>
        </div>

        <div>
          <ul>
            {/* Second set of list items */}
            <Link to="#" className="LinkWithoutUnderline">
              <li>How to Buy Travel Insurance</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>12 Tips for New Travelers</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>My Favorite Hostels</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>Solo Female Travel Tips</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>How to Pick a Travel Credit Card</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>Common Travel Scams to Avoid</li>
            </Link>
          </ul>
        </div>

        <div>
          <ul>
            {/* Third set of list items */}
            <Link to="#" className="LinkWithoutUnderline">
              <li>15 Ways to Work Overseas</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>8 Budget Vacation Ideas</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>Travel Hacking 101</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>How to Travel with a Theme</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>Stay in Europe More Than 90 Days</li>
            </Link>
            <Link to="#" className="LinkWithoutUnderline">
              <li>The Best Travel Credit Cards</li>
            </Link>
          </ul>

        </div>

      </div>

      <p style={{ margin: '0', padding: '0' }} className='pgenerals'>
        ************************************************  <nbsp></nbsp>
        <i className="fa-solid fa-earth-americas fa-lg" style={{ color: '#5c81c1' }}></i>
        <nbsp></nbsp> ************************************************
      </p>



      {/* Fifth container - Book your trip now */}
      <p className='pgeneral'>Book your trip now</p>
      <p className='pgeneralssss'>Below are my favorite companies to use when I travel.<br></br>They are always my starting point when I need to book a flight, hotel, tour, or train, or for meeting people!</p>

      <div className='fivemargin'>

        <div className='contfive'>

          <li>
            <Link to="https://www.skyscanner.com" className="LinkWithoutUnderline" target="_blank" rel="noopener noreferrer">
              Skyscanner
            </Link> is my favorite flight search engine. It searches small websites and budget airlines that larger search sites tend to miss. It is hands-down the number one place to start.
          </li>
          <li>
            <Link to="https://www.going.com" className="LinkWithoutUnderline" target="_blank" rel="noopener noreferrer">
              Going
            </Link> (formerly Scott’s Cheap Flights) finds incredible flight deals and sends them directly to your inbox. If you’re flexible with your dates and destinations, you can score some amazing deals and save hundreds of dollars in the process!
          </li>
          <li>
            <Link to="https://www.hostelworld.com" className="LinkWithoutUnderline" target="_blank" rel="noopener noreferrer">
              Hostelworld
            </Link> – This is the best hostel accommodation site out there, with the largest inventory, best search interface, and widest availability.
          </li>
          <li>
            <Link to="https://www.booking.com" className="LinkWithoutUnderline" target="_blank" rel="noopener noreferrer">
              Booking.com
            </Link> – The best all-around booking site. It constantly provides the cheapest and lowest rates and has the widest selection of budget accommodation. In all my tests, it’s always had the cheapest rates out of all the booking websites.
          </li>
          <li>
            <Link to="https://www.intrepidtravel.com" className="LinkWithoutUnderline" target="_blank" rel="noopener noreferrer">
              Intrepid Travel
            </Link> – If you want to do group tours, go with Intrepid. They offer good small-group tours that use local operators and leave a minimal environmental footprint. And, as a reader of this site, you’ll get exclusive discounts too!
          </li>
          <li>
            <Link to="https://www.getyourguide.com" className="LinkWithoutUnderline" target="_blank" rel="noopener noreferrer">
              Get Your Guide
            </Link> – This is a huge online marketplace for tours and excursions. It has tons of tour options in cities all around the world, including everything from cooking classes and walking tours to street art lessons!
          </li>
          <li>
            <Link to="https://www.safetywing.com" className="LinkWithoutUnderline" target="_blank" rel="noopener noreferrer">
              SafetyWing
            </Link> – This site offers convenient and affordable plans tailored to digital nomads and long-term travelers. It has cheap monthly plans, great customer service, and an easy-to-use claims process that makes it perfect for those on the road.
          </li>
          <li>
            <Link to="https://www.discovercars.com" className="LinkWithoutUnderline" target="_blank" rel="noopener noreferrer">
              Discover Cars
            </Link> – Discover Cars is a car rental aggregator that can help you find the best deals for your next road trip. It pulls data from over 8,000 car rental locations to ensure you always find a great deal!
          </li>
          <li>
            <Link to="https://www.trustedhousesitters.com" className="LinkWithoutUnderline" target="_blank" rel="noopener noreferrer">
              Trusted Housesitters
            </Link> – Trusted Housesitters is a platform that connects you with people in need of pet and house sitters. In exchange for looking after their pets or home, you’ll get access to free accommodation.
          </li>
          <li>
            <Link to="https://www.nomadicmatt.com/travel-tips/travel-credit-cards" className="LinkWithoutUnderline" target="_blank" rel="noopener noreferrer">
              Top Travel Credit Cards
            </Link> – Points are the best way to cut down travel expenses. Here’s my favorite point earning credit cards so you can get free travel!
          </li>



        </div>







      </div>





    </div>
  );
}

export default Home;


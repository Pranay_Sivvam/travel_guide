import React, { useState } from 'react';
import './placesincountries.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';

function HoiAn(args) {
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const book = () => alert("Booking Successful")

    const imageUrls = [
    'https://www.holidify.com/images/bgImages/HOI-AN.jpg',
    ];


    return (
        <div>
            {/* Images */}
            <div className='topdata'>
                <div className="image-container">
                    <h1>Hoi An</h1> <br></br>
                    {imageUrls.map((url, index) => (
                        <img key={index} src={url} alt={`Image ${index + 1}`}
                            style={{ width: '50%', height: '5%' }} />
                    ))}
                </div>
            </div>

            <br></br>

            {/* Data in points */}
            <div className='locationdata'>
                <h5><i class="fa-solid fa-location-dot"></i> Union Territory - Vietnam</h5>
                <h5><i class="fa-solid fa-indian-rupee-sign"></i> Packages :  15,200 onwards</h5>
                <h5><i class="fa-regular fa-star"></i> Rating :  4.5/5 (2023 ratings)</h5>
                <h5><i class="fa-solid fa-cloud"></i> Weather : 21 - 25°C </h5>
                <h5><i class="fa-solid fa-calendar-days"></i> Ideal duration : 1 day</h5>
                <h5> <i class="fa-solid fa-clock"></i> Best Time : February to June. The best time to visit this place is during the months of summer or springs.</h5>
            </div>

            <br></br>

            {/* Booking button */}
            <div>
                <div>
                    <Button color="danger" onClick={toggle}>
                        Book Now
                    </Button>
                    <Modal isOpen={modal} toggle={toggle} {...args}>
                        <ModalHeader toggle={toggle}>Hotel Booking</ModalHeader>
                        <ModalBody>


                            <Form>
                                <FormGroup>
                                    <Label for="namefull">
                                        Full Name
                                    </Label>
                                    <Input
                                        id="namefull"
                                        name="Full Name"
                                        placeholder="Enter your name here"
                                        type="text"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="noofpersons">
                                        No of persons
                                    </Label>
                                    <Input
                                        id="noofpersons"
                                        name="No of persons"
                                        placeholder="Enter number of persons to book room"
                                        type="text"
                                    />
                                </FormGroup>

                                <FormGroup check>
                                    <Input type="checkbox" />
                                    {' '}
                                    <Label check>
                                        Check me out
                                    </Label>
                                </FormGroup>
                                <Button onClick={book}>
                                <Link to="/">Book Now</Link>
                                </Button>
                            </Form>

                        </ModalBody>
                    </Modal>
                </div>
            </div>

            <br></br>

            {/* Place description */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>"Hoi An-City With Heritage"</h4>
                    <h6>Hoi An Tourism</h6> <br></br>
                    <p>One of the oldest cities in Asia, Hoi An is a town in central Vietnam which was declared as a World Heritage Site by UNESCO in 1999. Located south of Danang, Hoi An translates to 'a peaceful meeting place'. It is culturally and historically one of Vietnam's most important towns and also one of the richest. It is important to note that Hoi An does not have an airport or train station and can only be reached by road.

                       Being a trading port, the food of Hoi An is delightfully interesting and multi-cultural. The regional cuisine is served traditionally but also can be found to be served under the influence of Eastern Asia and other parts of South-Eastern Asia. A popular tourist activity here is cooking classes where tourists are taught about the local ingredients and can try their hand in preparing some traditional dishes.

                       Owing to being a former riverside port town, the Japanese and Chinese influence over the town's architecture stands tall till today. The Japanese merchant houses, the temples built by the Chinese and even the warehouses hold the memory of the confluence of traders from various communities across Asia.   </p>


                </div>
            </div>

            <br></br>

            {/* Must Know Main Points */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>Nightlife in Hoi An</h4>
                    <p>
                        <li>The nightlife in Hoi An consists of trendy street bars, bistros and chic lounges. Many of these establishments offer excellent live music as well. </li>

                        <li>The Ancient Town and Riverside is where you should go seeking your table for the night. Food is available for decent prices, and some of the best inexpensive beer is up for grabs till 3 in the morning.</li>

                        <li>In case you are hunting for some cheap clothes, knick-knacks and souvenirs, the night market at Nguyen Hoang Street is highly recommended.</li>

                    </p>

                </div>
            </div>


            {/* Back to Home button */}
            <div className='backth'>
            <Link to="/vietnam"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>
                
            </div>




        </div>
    )
}

export default HoiAn
import React, { useState } from 'react';
import './placesincountries.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';


function Singaporelocal(args) {
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const book = () => alert("Booking Successful")

    const imageUrls = [
        'https://www.holidify.com/images/cmsuploads/compressed/Resorts_World_Sentosa_viewed_from_the_Tiger_Sky_Tower2C_Sentosa2C_Singapore_-_20110131_20190606192041.jpg',
    ];


    return (
        <div>
            {/* Images */}
            <div className='topdata'>
                <div className="image-container">
                    <h1>Singapore</h1> <br></br>
                    {imageUrls.map((url, index) => (
                        <img key={index} src={url} alt={`Image ${index + 1}`}
                            style={{ width: '50%', height: '5%' }} />
                    ))}
                </div>
            </div>

            <br></br>

            {/* Data in points */}
            <div className='locationdata'>
                <h5><i class="fa-solid fa-location-dot"></i> Singapore</h5>
                <h5><i class="fa-solid fa-indian-rupee-sign"></i> Packages :  18,500 onwards</h5>
                <h5><i class="fa-regular fa-star"></i> Rating :  4.7 /5 (2023 ratings)</h5>
                <h5><i class="fa-solid fa-cloud"></i> Weather : 25 - 32°C </h5>
                <h5><i class="fa-solid fa-calendar-days"></i> Ideal duration : 3-4 days</h5>
                <h5> <i class="fa-solid fa-clock"></i> Best Time : February - April </h5>
            </div>

            <br></br>

            {/* Booking button */}
            <div>
                <div>
                    <Button color="danger" onClick={toggle}>
                        Book Now
                    </Button>
                    <Modal isOpen={modal} toggle={toggle} {...args}>
                        <ModalHeader toggle={toggle}>Hotel Booking</ModalHeader>
                        <ModalBody>


                            <Form>
                                <FormGroup>
                                    <Label for="namefull">
                                        Full Name
                                    </Label>
                                    <Input
                                        id="namefull"
                                        name="Full Name"
                                        placeholder="Enter your name here"
                                        type="text"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="noofpersons">
                                        No of persons
                                    </Label>
                                    <Input
                                        id="noofpersons"
                                        name="No of persons"
                                        placeholder="Enter number of persons to book room"
                                        type="text"
                                    />
                                </FormGroup>

                                <FormGroup check>
                                    <Input type="checkbox" />
                                    {' '}
                                    <Label check>
                                        Check me out
                                    </Label>
                                </FormGroup>
                                <Button onClick={book}>
                                    <Link to="/">Book Now</Link>
                                </Button>
                            </Form>

                        </ModalBody>
                    </Modal>
                </div>
            </div>

            <br></br>

            {/* Place description */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>"The Quintessential Cosmopolitan"</h4>
                    <h6>Singapore Tourism</h6> <br></br>
                    <p>Best described as a microcosm of modern Asia, Singapore is a melting pot of culture and history, and an extravaganza of culinary delights. Officially known as the Republic of Singapore, it is both a city and a country located in Southeast Asia. One of Asia's most visited destinations, Singapore is best described as an amalgam of a fast-paced life and an off-the-back-street inheritance.

                        Singapore is the quintessential cosmopolitan, having the highest religious diversity in any country. Spread 42 km (26 miles) east to west and 23 km (14 miles) north to south, today it boasts of the world's busiest port. Singapore has climbed to be one of Asia's hit-list destinations with its efficient and widespread transport system - whizzing in this country is just a matter of minutes!

                        The national pastime of Singaporeans is eating, followed closely by shopping. This 'City in a Garden' is a blend of cultures, combining different ideas, cuisines, new architectures going well with the gleaming hint of the old school. The incredible shopping malls, classy boutiques, departmental stores on Orchard Road, the exotic elements of Chinatown and Little India and the world-class nightlife span across the spotless land of Singapore.

                        Expensive with respect to South-eastern standards, the city offers a plethora of other options for entertainment such as Sentosa Island, Singapore Zoo, Singapore Botanic Garden, Marina Bay Sands, Tiger Balm Garden, and the Singapore Night Safari. With a picture-perfect skyline and city centre bustling with crowds of people, Singapore is one of the most popular travel destinations for a lot of reasons.</p>


                </div>
            </div>

            <br></br>

            {/* Must Know Main Points */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>Must Know Before You Travel to Singapore</h4>
                    <p>
                        Singapore Tourist Pass (STP) :
                        <li>It is a special EZ-Link stored-value card allowing unlimited travel in MRT and Bus for one, two, and three days. It can be bought at TransitLink Ticket Office at selected MRT stations.
                            Children above 0.9m in height and below 7 years can travel free on MRT and bus. Apply for a Child Concession Card at TransitLink Ticket Offices.</li>

                        Tax Refund :
                        <li>If you purchase anything above USD 100 (including GST), you can claim a 7% GST refund. Look out for the Tax Free shopping logo or a sign displayed at the shop you are buying from. You can have 3 same-day receipts to meet the minimum purchase requirement of USD 100. The refund can be claimed via Electronic Tourist Refund (eTRS) self-help kiosk at the airport or via Global Blue or Tourego Mobile Applications (App).</li>

                        Wifi & Connectivity :
                        <li>There is a free public Wifi service across Singapore. Download the Wireless@SGx App to auto connect.</li>
                        No Smoking Areas:
                        <li>Smoking is not allowed in air-conditioned places, and your tobacco products should not exceed 4gm. Smoke only at designated smoking areas.</li>

                    </p>


                    <h6>Exchanging Money in Singapore </h6>
                    <p>
                        <li>Currency exchange in Singapore is done in banks as well as currency exchange booths found in almost every shopping mall. Generally, these booths offer better rates, opening hours and much faster service than banks.</li>
                        <li>The Mustafa Mall, which is open 24 hours, accepts almost any currency at a very good rate with equal competitiveness as the small shops located at Change Alley next to Raffles Place MRT.</li>
                        <li>Rates at the airport are not as good as in the city, so avoid exchanging there.</li>
                        <li>The ATMs are easily available throughout Singapore in large numbers.</li>
                    </p>

                    <h6>Currency in Singapore</h6>
                    <p>
                        <li>Singapore's unit of currency is the Singapore Dollar (SGD), locally referred to as the 'Singdollar'. It is made up of 100 cents using coins of 5, 10, 20 and 50 cents, along with notes in the denomination of SGD 2, 5, 10 and 50, 100, 500 and 1000. </li>
                        <li>Cards: Almost all the major credit card brands are widely accepted in Singapore including Visa and MasterCard (although a 3 % surcharge may be charged by some shops, taxis may charge up to 15 %).
                            Traveller's cheques are usually not accepted, however, can be cashed at most of the exchange booths.
                            EZ-Link and Nets Flash Pay cards are valid in case of some convenience stores and fast-food chains.</li>
                        <li>ATMs: ATMs are widely available at banks, malls, MRT stations and commercial areas.</li>
                    </p>


                    <h6>Daily Budget for Singapore </h6>
                    <p>
                        Singapore, in general, is an expensive country by the standards of the Asian countries. Although the city caters to the taste and interest of each kind of tourist, the minimum budget for backpackers, mid-rangers and luxury travellers are higher in comparison to the neighbouring countries. The cost of travelling in Singapore per day and per person (excluding hotel expense) is:
                        <li>Budget Travel: Less than SGD 200</li>
                        <li>Comfortable Travel: SGD 200 - 400</li>
                        <li>Luxury Travel: SGD 400 plus</li>
                    </p>

                    <h6>Religion of Singapore </h6>
                    <p>
                        Being a multi-religious country, Singapore does not have a state-regulated religion which the citizens are supposed to follow. It is home to 10 religions, out of which Buddhism, Hinduism, Islam and Christianity are the primary religions, while Zoroastrianism, Judaism, Sikhism, Jainism and others form the minority cluster. The Lion City is the ultimate melting pot, with the locals celebrating all festivals pompously, irrespective of the religion they follow.

                    </p>

                    <h6>Language of Singapore</h6>
                    <p>
                        <li>There are four official languages of Singapore: English, Malay, Mandarin Chinese and Tamil. Yes, Singapore language is as diverse and multi-cultural as its people! English is the most widely spoken language (primarily by the population below the age of 50), and the medium of instructions in school. English is also the language of business and government in Singapore, based on British English. </li>
                        <li>A unique and widely spoken language in Singapore is the Singlish. It is primarily the colloquial form of English, having a distinct accent, and ignoring the basic standards of English grammar. Having a jumble of local slang and expressions of various languages and dialects of Singapore, speaking in Singlish is seen as a mark of being truly local! </li>
                        <li>The major portion of the literate population in Singapore is bilingual, with English and Mandarin being most commonly spoken. Interestingly, all the schools in the city teach the language of the child's parentage, along with English, to ensure the child stay in touch with the traditional roots.</li>
                    </p>


                    <h6>History of Singapore</h6>
                    <p>
<li>According to the historical records, the story of Singapore goes as back as the third century, then referred to as 'island at the end'. It was between 1298 and 1299 AD that the first settlement was established when the city came to be known as Tumasik or Sea Town. According to a legend, the current name of the country came into being during the 14th century when a prince of Palembang (capital of Srivijaya) saw an animal he had never seen before while on his hunting trip. Taking it as a good omen, he named the city as 'Singapura' or the 'lion city' derived from the Sanskrit words 'Simha' meaning 'lion' and 'pura' meaning.</li>
<li>Soon, the city located at the tip of the Malay Peninsula (natural meeting point of various sea routes) became the major trading hub for Arabs dhows, Chinese junk, Portuguese battleships and Buginese schooners. However, the modern city of Singapore was founded in the 19th century by Sir Thomas Stanford Raffles. After passing from various hands of power, the island was handed over to British in 1946 as its crown colony. The revolution for independence came about in 1959 when the country's first general elections were held and Lee Kuan Yew became first Prime Minister of Singapore. One can witness the imprints of the island's rich past and struggle at various museums, monuments and memorials.</li>

                           </p>


                </div>
            </div>


            {/* Back to Home button */}
            <div className='backth'>
                <Link to="/singapore"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>
            </div>




        </div>
    )
}

export default Singaporelocal
import React from 'react'

function Blog() {
  return (
    <div>
   <h2>Latest From The Blog</h2> <br></br>
      <div className='conttwo'>
        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2023/11/aroundtoursfeature-760x440.jpg' alt='RoamRover' />
          <p>The Best Place to Take Audio Tours</p>
        </div>

        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2018/12/giftguide2.jpg' alt='RoamRover' />
          <p>My Top Gifts for Travelers for the Holidays</p>
        </div>

        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2020/08/gearguide5-380x220.jpg' alt='RoamRover' />
          <p>My Favorite Gear for Travelers</p>
        </div>
      </div>

      <br></br>

      <div className='conttwo'>
        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2020/12/workfromhome1b-380x220.jpg' alt='RoamRover' />
          <p>Work from Home Gift Guide: <br></br>18 Amazing Gifts for Remote Workers</p>
        </div>

        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2023/10/bestccsforstudents-760x440.jpg' alt='RoamRover' />
          <p>The Best Credit Cards for Students Who <br></br>Want to Travel</p>
        </div>

        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2023/09/budapestwhere-760x440.jpeg' alt='RoamRover' />
          <p>Where to Stay in Budapest: <br></br>The Best Neighborhoods For Your Visit</p>
        </div>
      </div>
      <br></br> <br></br>

      <div className='conttwo'>
        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2023/10/vancouverstay-335x200.jpeg' alt='RoamRover' />
          <p>Where to Stay in Vancouver: <br></br> The Best Neighborhoods for Your Visit
</p>
        </div>

        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2023/10/ccinsure-335x200.jpeg' alt='RoamRover' />
          <p>The Best Credit Cards for Travel Insurance</p>
        </div>

        <div>
          <img className='cont2img' src='https://www.nomadicmatt.com/wp-content/uploads/2023/10/venicewalks-335x200.jpeg' alt='RoamRover' />
          <p>The Best Walking Tours in Venice</p>
        </div>
      </div>












        
    </div>
  )
}

export default Blog
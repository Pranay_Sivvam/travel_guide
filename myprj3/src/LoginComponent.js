// Login.js
import React, { useState, useEffect } from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Input, Form, Label, FormGroup } from "reactstrap";
import { Link } from "react-router-dom"; // Import Link from react-router-dom
import axios from "axios";

const LoginComponent = () => {
  const [modal, setModal] = useState(true);
  const toggleModal = () => setModal(!modal);

  const [EmailAddress, setUsername] = useState("");
  const [Password, setPassword] = useState("");
  const [message, setMessage] = useState("");

  useEffect(() => {
  }, []);

  const handleLogin = async () => {
    try {
      const response = await axios.get(`http://localhost:3006/login/${EmailAddress}/${Password}`);
      if (response.data) {
        setMessage("Login successful");

        window.location.href = "/RegistrationForm";
      } else {
        setMessage("Login unsuccessful");
      }
    } catch (error) {
      console.error("Error logging in:", error);
    }
  };

  return (
    <div>


      <Modal isOpen={modal} toggle={toggleModal} className="custom-modal">
        <ModalHeader toggle={toggleModal} className="custom-modal-header">
          Login
        </ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label for="EmailAddress">Email Address</Label>
              <Input
                type="text"
                name="Email Address"
                placeholder="Email"
                value={EmailAddress}
                onChange={(e) => setUsername(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="Password">Password</Label>
              <Input
                type="password"
                name="password"
                placeholder="Password"
                value={Password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter className="custom-modal-footer">
          <Button color="primary" onClick={handleLogin} className="btn btn-dark">
            <Link to="/navafter">Explore Now</Link>
          </Button>
          <Link
            to="/RegistrationForm"
            className="btn btn-default border w-100 bg-light rounded-0 text-decoration-none"
          >
            Create Account
          </Link>
        </ModalFooter>
      </Modal>

      {message && (
        <div>
          <h3>{message}</h3>
        </div>
      )}
    </div>
  );
};

export default LoginComponent;







// import React, { useState, useEffect } from "react";
// import {
//   Modal,
//   ModalHeader,
//   ModalBody,
//   ModalFooter,
//   Button,
//   Input,
//   Form,
//   Label,
//   FormGroup,
// } from "reactstrap";
// import { Link, useLocation } from "react-router-dom"; // Import useLocation hook
// import axios from "axios";
// import { useNavigate } from "react-router-dom";
// // import './LoginComponent.css';

// const LoginComponent = () => {
//   const [modal, setModal] = useState(true); // Set the modal to open by default
//   const toggleModal = () => setModal(!modal);

//   const [EmailAddress, setUsername] = useState("");
//   const [Password, setPassword] = useState("");
//   const [message, setMessage] = useState("");

//   const navigate = useNavigate();
//   const location = useLocation(); // Get the current location

//   useEffect(() => {
//     // You can add any initial logic here
//   }, []);

//   const handleLogin = async () => {
//     try {
//       const response = await axios.get(
//         `http://localhost:3003/login/${EmailAddress}/${Password}/`
//       );
//       if (response.data) {
//         setMessage("Login successful");
//         navigate("/Navbar"); // Redirect to the homepage
//       } else {
//         setMessage("Login unsuccessful");
//       }
//     } catch (error) {
//       console.error("Error logging in:", error);
//     }
//   };

//   // Check if the current route is the home page ("/home") and don't render the login form
//   if (location.pathname === "/home") {
//     return null;
//   }

//   return (
//     <div>
//       <Modal isOpen={modal} toggle={toggleModal}  className="custom-modal">
//         <ModalHeader toggle={toggleModal} className="custom-modal-header">
//           Login
//         </ModalHeader>
//         <ModalBody>
//           <Form>
//             <FormGroup>
//               <Label for="EmailAddress">Email Address</Label>
//               <Input
//                 type="text"
//                 name="Email Address"
//                 placeholder="Email"
//                 value={EmailAddress}
//                 onChange={(e) => setUsername(e.target.value)}
//               />
//             </FormGroup>
//             <FormGroup>
//               <Label for="Password">Password</Label>
//               <Input
//                 type="password"
//                 name="password"
//                 placeholder="Password"
//                 value={Password}
//                 onChange={(e) => setPassword(e.target.value)}
//               />
//             </FormGroup>
//           </Form>
//         </ModalBody>
//         <ModalFooter className="custom-modal-footer">
//           <Button
//             color="primary"
//             onClick={handleLogin}
//             className="custom-button"
//           >
//             Login
//           </Button>
//           <Link
//             to="/RegistrationForm"
//             className="btn btn-default border w-100 bg-light rounded-0 text-decoration-none"
//           >
//             Create Account
//           </Link>
//         </ModalFooter>
//       </Modal>

//       {message && (
//         <div>
//           <h3>{message}</h3>
//         </div>
//       )}
//     </div>
//   );
// };
// // custom-modal {
// //   background-image: url('path/to/your-background-image.jpg');
// //   background-size: cover; /* Adjust as needed */
// //   background-position: center; /* Adjust as needed */
// //   /* Other styles for the modal container */
// // }

// export default LoginComponent;

// RegistrationForm.js
import React, { useState } from 'react';
import axios from 'axios';
import './Registration.css';
import { Link } from 'react-router-dom';

function RegistrationForm() {
    const [Firstname, setFirstname] = useState('');
    const [Lastname, setLastname] = useState('');
    const [EmailAddress, setEmailAddress] = useState('');
    const [PhoneNumber, setPhoneNumber] = useState('');
    const [Password,setPassword] = useState('');
    const [ConfirmPassword,setConfirmPassword] =useState('')
    const [DreamDestination,setDreamDestination] = useState('');
    const [Pincode, setPincode] = useState('');


    const handleRegister = async (event) => {
        event.preventDefault();
        setFirstname("")
        setLastname("")
        setEmailAddress("")
        setPhoneNumber("")
        setPassword("")
        setConfirmPassword("")
        setDreamDestination("")
        setPincode("")
        
        try {
            const response = await axios.post('http://localhost:3006/register', {
                Firstname,
                Lastname,
                EmailAddress,
                PhoneNumber,
                Password,
                ConfirmPassword,
                DreamDestination,
                Pincode,
            });



            if (response.data) {
                alert('Registration successful');
            } else {
                alert('Registration failed');
            }
        } catch (error) {
            console.error(error);
        }
    };

    return (
<div className='Bodys'>
        <div className="Registration-Form">
            
            <form onSubmit={handleRegister} id="form">
                <h1>REGISTRATION FORM</h1>
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label htmlFor="Firstname">Firstname:</label>
                        <input
                            type="text"
                            class="form-control"
                            id="Firstname"
                            value={Firstname}
                            onChange={(e) =>setFirstname(e.target.value)}
                            required
                        />
                    </div>
                    <div class="col-md-6 mb-3">
                    <label htmlFor="Lastname">Lastname:</label>
                    <input
                        type="text"
                        class="form-control"
                        id="Lastname"
                        value={Lastname}
                        onChange={(e) => setLastname(e.target.value)}
                        required
                        // style={{ marginLeft: 75 }}
                    />
                    </div>
                </div>
                <div className="form-row">
                    <div class="col-md-6 mb-3">
                    <label htmlFor="Email">EmailAddress:</label>
                    <input
                        type="text"
                        class="form-control"
                        id="Email"
                        value={EmailAddress}
                        onChange={(e) => setEmailAddress(e.target.value)}
                        required
                        // style={{ marginLeft: 75 }}
                    />
                    </div>
                <div class="col-md-6 mb-3">
                    <label htmlFor="PhoneNumber">PhoneNumber:</label>
                    <input
                        type="text"
                        class="form-control"
                        id="PhoneNumber"
                        value={PhoneNumber}
                        onChange={(e) => setPhoneNumber(e.target.value)}
                        required
                        // style={{ marginLeft: 75 }}
                    />
                     </div>
                </div>
                <div className="form-row">
                <div class="col-md-6 mb-3">
                    <label htmlFor="Password">Password:</label>
                    <input
                        type="text"
                        class="form-control"
                        id="Password"
                        value={Password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                        // style={{ marginLeft: 75 }}
                    />
                     </div>
                <div class="col-md-6 mb-3">
                    <label htmlFor="ConfirmPassword">ConfirmPassword:</label>
                    <input
                        type="text"
                        class="form-control"
                        id="ConfirmPassword"
                        value={ConfirmPassword}
                        onChange={(e) => setConfirmPassword(e.target.value)}
                        required
                        // style={{ marginLeft: 75 }}
                    />
                     </div>
                </div>
                <div className="form-row">
                <div class="col-md-6 mb-3">
                    <label htmlFor="Address">Dream Destination:</label>
                    <input
                        type="text"
                        class="form-control"
                        id="Address"
                        value={DreamDestination}
                        onChange={(e) => setDreamDestination(e.target.value)}
                        required
                    />
                     </div>
                <div class="col-md-6 mb-3">
                    <label htmlFor="Pincode">Pincode:</label>
                    <input
                        type="text"
                        class="form-control"
                        id="Pincode"
                        value={Pincode}
                        onChange={(e) => setPincode(e.target.value)}
                        required
                    />
                     </div>
                </div>
                <button type="submit" class="btn btn-dark" id='buttonreg' ><Link to="/login">Submit</Link></button> <br></br>

                <button type="submit" class="btn btn-dark" id='buttonreg' ><Link to="/login">Already have an account! Login</Link></button>

            </form>
        </div>
        </div>
    );
}
export default RegistrationForm;
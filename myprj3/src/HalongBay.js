import React, { useState } from 'react';
import './placesincountries.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';

function HalongBay(args) {
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const book = () => alert("Booking Successful")

    const imageUrls = [
        'https://www.holidify.com/images/bgImages/HALONG-BAY.jpg',
    ];


    return (
        <div>
            {/* Images */}
            <div className='topdata'>
                <div className="image-container">
                    <h1>Halong Bay</h1> <br></br>
                    {imageUrls.map((url, index) => (
                        <img key={index} src={url} alt={`Image ${index + 1}`}
                            style={{ width: '50%', height: '5%' }} />
                    ))}
                </div>
            </div>

            <br></br>

            {/* Data in points */}
            <div className='locationdata'>
                <h5><i class="fa-solid fa-location-dot"></i> Union Territory - Vietnam</h5>
                <h5><i class="fa-solid fa-indian-rupee-sign"></i> Packages :  15,260 onwards</h5>
                <h5><i class="fa-regular fa-star"></i> Rating :  4.2/5 (2023 ratings)</h5>
                <h5><i class="fa-solid fa-cloud"></i> Weather : 17 - 21°C </h5>
                <h5><i class="fa-solid fa-calendar-days"></i> Ideal duration : 1 day</h5>
                <h5> <i class="fa-solid fa-clock"></i> Best Time : During the winter and spring months.</h5>
            </div>

            <br></br>

            {/* Booking button */}
            <div>
                <div>
                    <Button color="danger" onClick={toggle}>
                        Book Now
                    </Button>
                    <Modal isOpen={modal} toggle={toggle} {...args}>
                        <ModalHeader toggle={toggle}>Hotel Booking</ModalHeader>
                        <ModalBody>


                            <Form>
                                <FormGroup>
                                    <Label for="namefull">
                                        Full Name
                                    </Label>
                                    <Input
                                        id="namefull"
                                        name="Full Name"
                                        placeholder="Enter your name here"
                                        type="text"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="noofpersons">
                                        No of persons
                                    </Label>
                                    <Input
                                        id="noofpersons"
                                        name="No of persons"
                                        placeholder="Enter number of persons to book room"
                                        type="text"
                                    />
                                </FormGroup>

                                <FormGroup check>
                                    <Input type="checkbox" />
                                    {' '}
                                    <Label check>
                                        Check me out
                                    </Label>
                                </FormGroup>
                                <Button onClick={book}>
                                <Link to="/">Book Now</Link>
                                </Button>
                            </Form>

                        </ModalBody>
                    </Modal>
                </div>
            </div>

            <br></br>

            {/* Place description */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>"Breathtaking karst formations and scenic boat cruises"</h4>
                    <h6>Halong Bay Tourism</h6> <br></br>
                    <p>Characterised by towering karst limestone pillars and small islets of various shapes and sizes amidst crystal blue waters, Halong Bay's ethereal beauty is a sight to behold. Located in northern Vietnam, it is the most popular tourist spot this side of the country. Halong Bay, where 'Halong' translates to 'where the dragon descends into the sea', had a part of it designated as a World Heritage Site in 1994. Halong City is the main entry point to the bay.

                        The most common way of exploring the region is via a cruise or day-trip boats, where tourists laze around in the boats while cruising among the limestone pillars and a number of islets. Many cruises and day-trips also include an island drop off and cave explorations. There are some floating villages as well which allow visitors to come and interact with the locals, try their food and buy knick-knacks.

                        Kayaking is another activity which has vastly become popular in recent years. One can kayak near and around the limestone pillars on a bright sunny day. Few caves also allow for exploring by kayaking which is an experience highly recommended.</p>


                </div>
            </div>

            <br></br>

            {/* Must Know Main Points */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>Must Know Before You Travel to Halong Bay</h4>
                    <p>
                        <li>The best way to explore Halong Bay is by cruise. There are day cruises and cruises with onboard stays for multiple nights available in various budget categories. </li>

                        <li>Be sure to book these cruises well in advance, especially during the peak season, as they can get sold out quickly.</li>

                        <li>Tours to Halong Bay generally begin from Hanoi, but you can also head to Halong Bay from other nearby towns or cities. In addition, buses are regularly available to Halong Bay from most towns and cities.</li>

                        <li>It is advisable to seek help/advice from your hotel management about tours and cruises in Halong Bay. Generally, the hotel patrons can give a complete guide for the same.</li>

                        <li>Be sure to carry cash when traveling to Halong Bay.</li>

                        <li>As Island stops and water activities are a part of most cruises, be sure to carry appropriate clothing as needed.</li>

                        <li>Also, carry hats, sunscreens, and any such lotions and medications as needed, as these are not easy to come by when out in the bay.</li>

                    </p>

                </div>
            </div>


            {/* Back to Home button */}
            <div className='backth'>
            <Link to="/vietnam"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>
                
            </div>




        </div>
    )
}

export default HalongBay;
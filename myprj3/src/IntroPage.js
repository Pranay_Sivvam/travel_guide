// IntroPage.js
import React from 'react';
import { useSpring, animated } from 'react-spring';
import './IntroPage.css'; // Create this CSS file for styling
import { Link } from 'react-router-dom';
import Footer from './Footer';

const IntroPage = () => {
  const fadeIn = useSpring({
    opacity: 1,
    from: { opacity: 0 },
    config: { duration: 1000 },
  });

  return (
    <animated.div className="intro-container" style={fadeIn}>
      <h1 className='head'>RomeRover</h1> <br></br>
      <p>Your passport to unparalleled travel experiences!</p>
      <p>Embark on a journey where every destination is a stroke of wanderlust waiting to be painted.</p>
      <br></br>
      <Link to="/login"><button>Explore Now</button></Link>
      
    </animated.div>
    
  );
};

export default IntroPage;

import React from 'react'
import './Vietnam.css'
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import Navcountry from './Navcountry';
import {
    Button, Row, Col, CardTitle, CardText, CardGroup,
    Card,
    CardImg,
    CardBody,
    CardSubtitle,
} from 'reactstrap';

function Vietnam() {
  return (
      <div>
          <Navcountry/>
          <div className='totalcountry'>
              <div className='country'>
                  <h2>VIETNAM - A Timeless Charm</h2> <br></br>
<p>Welcome to Vietnam, a country that beckons travelers with its enchanting blend of rich history, vibrant culture, and breathtaking landscapes. As you embark on your journey through this Southeast Asian gem, you'll find yourself immersed in a tapestry of timeless beauty.</p>

<p>Historical Marvels:
<li>Vietnam's history unfolds like a living museum. From the ancient wonders of Hanoi, where historic temples whisper tales of the past, to the Imperial City of Hue, a testament to the country's regal legacy, every step resonates with the echoes of a bygone era. Explore the UNESCO World Heritage Site, Ha Long Bay, where mystical limestone karsts rise majestically from emerald waters, offering a glimpse into the geological wonders that have shaped Vietnam.</li>
</p>


<p>Cultural Riches:
<li>Vietnam's cultural canvas is painted with a kaleidoscope of traditions. Witness the captivating water puppetry, partake in colorful festivals that illuminate the streets, and engage with age-old customs that have stood the test of time. In Hoi An's bustling streets or during a traditional Vietnamese performance, the spirit of Vietnam comes alive, inviting you to become a part of its living heritage.</li></p>


 </div>



          </div>
          <br></br>

          <div className='countrynamecards'>
              <h2>Top Places to visit in Vietnam</h2> <br></br>
          </div>
          <div className='alighmiddle'>
              <div className='cards'>
                  <CardGroup>
                      <Card>
                          <CardImg
                              alt="Card image cap"
                              src="https://www.holidify.com/images/bgImages/HALONG-BAY.jpg"
                              top
                              width="80%"
                          />
                          <CardBody>
                              <CardTitle tag="h5">
                              Halong Bay
                              </CardTitle>
                              {/* <CardSubtitle
                                  className="mb-2 text-muted"
                                  tag="h6"
                              >
                                  Card subtitle
                              </CardSubtitle>
                              <CardText>
                                  This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                              </CardText> */}
                              <Button>
                              <Link to="/holongbay">See More</Link>
                              </Button>
                          </CardBody>
                      </Card>
                      <Card>
                          <CardImg
                              alt="Card image cap"
                              src="https://www.holidify.com/images/bgImages/HOI-AN.jpg"
                              top
                              width="80%"
                          />
                          <CardBody>
                              <CardTitle tag="h5">
                                  Hoi An</CardTitle>
                              {/* <CardSubtitle
                                  className="mb-2 text-muted"
                                  tag="h6"
                              >
                                  Card subtitle
                              </CardSubtitle>
                              <CardText>
                                  This card has supporting text below as a natural lead-in to additional content.
                              </CardText> */}
                              <Button>
                              <Link to="/hoian">See More</Link>
                              </Button>
                          </CardBody>
                      </Card>
                      <Card>
                          <CardImg
                              alt="Card image cap"
                              src="https://www.holidify.com/images/bgImages/HO-CHI-MINH-CITY.jpg"
                              top
                              width="80%"
                          />
                          <CardBody>
                              <CardTitle tag="h5">
                              Ho Chi Minh City
                              </CardTitle>
                              {/* <CardSubtitle
                                  className="mb-2 text-muted"
                                  tag="h6"
                              >
                                  Card subtitle
                              </CardSubtitle>
                              <CardText>
                                  This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.
                              </CardText> */}
                              <Button>
                              <Link to="/hochiminh">See More</Link>
                              </Button>
                          </CardBody>
                      </Card>


                  </CardGroup>

              </div>
              <div>

              </div>

          </div> <br></br>

          <div className='alighmiddle'>
              <div className='cards'>
                  <CardGroup>
                      <Card>
                          <CardImg
                              alt="Card image cap"
                              src="https://www.holidify.com/images/bgImages/NHA-TRANG.jpg"
                              top
                              width="80%"
                          />
                          <CardBody>
                              <CardTitle tag="h5">
                              Nha Trang
                              </CardTitle>
                              {/* <CardSubtitle
                                  className="mb-2 text-muted"
                                  tag="h6"
                              >
                                  Card subtitle
                              </CardSubtitle>
                              <CardText>
                                  This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                              </CardText> */}
                              <Button>
                              <Link to="/nhatrang">See More</Link>
                              </Button>
                          </CardBody>
                      </Card>
                      <Card>
                          <CardImg
                              alt="Card image cap"
                              src="https://www.holidify.com/images/bgImages/PHONG-NHA-KE-BANG-NATIONAL-PARK.jpg"
                              top
                              width="80%"
                          />
                          <CardBody>
                              <CardTitle tag="h5">
                              Phong Nha-Ke Bang</CardTitle>
                              {/* <CardSubtitle
                                  className="mb-2 text-muted"
                                  tag="h6"
                              >
                                  Card subtitle
                              </CardSubtitle>
                              <CardText>
                                  This card has supporting text below as a natural lead-in to additional content.
                              </CardText> */}
                              <Button>
                              <Link to="/phongnhakebang">See More</Link>
                              </Button>
                          </CardBody>
                      </Card>
                      <Card>
                          <CardImg
                              alt="Card image cap"
                              src="https://www.holidify.com/images/bgImages/DA-NANG.jpg"
                              top
                              width="80%"
                          />
                          <CardBody>
                              <CardTitle tag="h5">
                              Da Nang
                              </CardTitle>
                              {/* <CardSubtitle
                                  className="mb-2 text-muted"
                                  tag="h6"
                              >
                                  Card subtitle
                              </CardSubtitle>
                              <CardText>
                                  This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.
                              </CardText> */}
                              <Button>
                              <Link to="/danang">See More</Link>
                              </Button>
                          </CardBody>
                      </Card>


                  </CardGroup>

              </div>
              <div>

              </div>

          </div> <br></br>

<div className='alighmiddle'>
  <div className='cards'>
      <CardGroup>
          <Card>
              <CardImg
                  alt="Card image cap"
                  src="https://www.holidify.com/images/bgImages/PHU-QUOC-ISLAND.jpg"
                  top
                  width="80%"
              />
              <CardBody>
                  <CardTitle tag="h5">
                  Phu Quoc Island
                  </CardTitle>
                  {/* <CardSubtitle
                      className="mb-2 text-muted"
                      tag="h6"
                  >
                      Card subtitle
                  </CardSubtitle>
                  <CardText>
                      This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                  </CardText> */}
                  <Button>
                  <Link to="/phuquoc">See More</Link>
                  </Button>
              </CardBody>
          </Card>
          <Card>
              <CardImg
                  alt="Card image cap"
                  src="https://www.holidify.com/images/bgImages/NINH-BINH.jpg"
                  top
                  width="80%"
              />
              <CardBody>
                  <CardTitle tag="h5">
                  Ninh Binh</CardTitle>
                  {/* <CardSubtitle
                      className="mb-2 text-muted"
                      tag="h6"
                  >
                      Card subtitle
                  </CardSubtitle>
                  <CardText>
                      This card has supporting text below as a natural lead-in to additional content.
                  </CardText> */}
                  <Button>
                  <Link to="/ninhbinh">See More</Link>
                  </Button>
              </CardBody>
          </Card>
          <Card>
              <CardImg
                  alt="Card image cap"
                  src="https://www.holidify.com/images/bgImages/DA-LAT.jpg"
                  top
                  width="80%"
              />
              <CardBody>
                  <CardTitle tag="h5">
                  Dalat
                  </CardTitle>
                  {/* <CardSubtitle
                      className="mb-2 text-muted"
                      tag="h6"
                  >
                      Card subtitle
                  </CardSubtitle>
                  <CardText>
                      This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.
                  </CardText> */}
                  <Button>
                  <Link to="/dalat">See More</Link>
                  </Button>
              </CardBody>
          </Card>


      </CardGroup>

  </div>
  <div>

  </div>

</div> <br></br>


{/* Back to Home button */}
<div className='backth'>
          <Link to="/Vietnam"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>
              
          </div>

      </div>
  )
}


export default Vietnam
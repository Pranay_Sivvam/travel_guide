import React from 'react'
import './Thailand.css'
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import Navcountry from './Navcountry';
import {
    Button, Row, Col, CardTitle, CardText, CardGroup,
    Card,
    CardImg,
    CardBody,
    CardSubtitle,
} from 'reactstrap';

function Thailand() {
    return (
        <div>
                        <Navcountry/>

            <div className='totalcountry'>
                <div className='country'>
                    <h2>Thailand - Where Every Moment is a New Adventure</h2> <br></br>
                    <p>Thailand is a Southeast Asian country and one of Asia’s most popular tourist destinations, with stunning beaches, vibrant nightlife, and a backpacker-friendly atmosphere. Sharing borders with Myanmar, Laos, Cambodia, and Malaysia, it is one of the top countries for exploring this region. Thailand is full of immersive experiences, whether you're seeking vibrant city life in Bangkok, historical sites in Ayutthaya, trekking through the jungles of Chiang Mai or enjoying the repose of the Phi Phi Islands.</p>
                    <p>One of the top beach holiday destinations in the world, Thailand is home to destinations like Phuket, Koh Phi Phi, Krabi, and Koh Samui. These offer breathtaking beaches, perfect for sunbathing, swimming, snorkeling, and water sports. Thailand also boasts an unforgettable nightlife, particularly in cities like Bangkok, Pattaya, and Phuket. From trendy rooftop bars with panoramic city views to vibrant night markets and nightclubs pumping out music until the early hours, there is something for everyone.</p>
                    <p>Thailand is a country steeped in rich cultural heritage with a history spanning thousands of years. Visit its opulent temples, such as Wat Arun and Wat Phra Kaew, to witness intricate architecture and ornate decorations along with experiencing traditional Buddhist practices. Thailand is also a popular backpacker-friendly destination due to its affordability, accessibility, and welcoming atmosphere. Places like Bangkok's Khao San Road and Chiang Mai's Nimman Road are well-known hubs for backpackers.</p>
                    <p>Thai cuisine is beloved globally for its bold flavors, aromatic spices, and diverse dishes. From savory favorites like pad Thai, green curry, and tom yum soup to sweet treats like sticky mango rice, Thailand's culinary offerings delight food lovers. Encompassing lush mountains, fertile plains, tropical rainforests, and pristine coastlines, Thailand has to be visited to be experienced.</p>

                </div>


  


            </div>
            <br></br>

            <div className='countrynamecards'>
                <h2>Top Places to visit in Thailand</h2> <br></br>
            </div>
            <div className='alighmiddle'>
                <div className='cards'>
                    <CardGroup>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/BANGKOK.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Bangkok
                                </CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                </CardText> */}
                                <Button>
                                    <Link to="/bangkok">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>



                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/cmsuploads/compressed/katabeach_20191021170831.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Katabeach - Phuket
                                </CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This card has supporting text below as a natural lead-in to additional content.
                                </CardText> */}
                                <Button>
                                    <Link to="/phuket">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>



                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/CHIANG-MAI.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Chiang-Mai
                                </CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.
                                </CardText> */}
                                <Button>
                                    <Link to="/chiangmai">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>


                    </CardGroup>

                </div>
                <div>

                </div>

            </div> <br></br>

            <div className='alighmiddle'>
                <div className='cards'>
                    <CardGroup>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/PHI-PHI-ISLANDS.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Phi Phi Islands
                                </CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                </CardText> */}
                                <Button>
                                    <Link to="/phiphi">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>


                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/KRABI.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Krabi</CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This card has supporting text below as a natural lead-in to additional content.
                                </CardText> */}
                                <Button>
                                    <Link to="/krabi">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>


                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/AYUTTHAYA.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Ayutthaya
                                </CardTitle>
                                {/* <CardSubtitle
                                    className="mb-2 text-muted"
                                    tag="h6"
                                >
                                    Card subtitle
                                </CardSubtitle>
                                <CardText>
                                    This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.
                                </CardText> */}
                                <Button>
                                    <Link to="/ayutthaya">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>


                    </CardGroup>

                </div>
                <div>

                </div>

            </div> <br></br>



            <div className='alighmiddle'>
                <div className='cards'>
                    <CardGroup>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/KOH-TAO.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Koh Tao 
                                </CardTitle>
                                {/* <CardSubtitle
                        className="mb-2 text-muted"
                        tag="h6"
                    >
                        Card subtitle
                    </CardSubtitle>
                    <CardText>
                        This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                    </CardText> */}
                                <Button>
                                    <Link to="/kohtao">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/HUA-HIN.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Hua-Hin</CardTitle>
                                {/* <CardSubtitle
                        className="mb-2 text-muted"
                        tag="h6"
                    >
                        Card subtitle
                    </CardSubtitle>
                    <CardText>
                        This card has supporting text below as a natural lead-in to additional content.
                    </CardText> */}
                                <Button>
                                    <Link to="/huahin">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardImg
                                alt="Card image cap"
                                src="https://www.holidify.com/images/bgImages/PATTAYA.jpg"
                                top
                                width="80%"
                            />
                            <CardBody>
                                <CardTitle tag="h5">
                                    Pattayya
                                </CardTitle>
                                {/* <CardSubtitle
                        className="mb-2 text-muted"
                        tag="h6"
                    >
                        Card subtitle
                    </CardSubtitle>
                    <CardText>
                        This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.
                    </CardText> */}
                                <Button>
                                    <Link to="/pattayya">See More</Link>
                                </Button>
                            </CardBody>
                        </Card>


                    </CardGroup>

                </div>
                <div>

                </div>

            </div> <br></br>

            {/* Back to Home button */}
            <div className='backth'>
                <Link to="/Thailand"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>

            </div>

        </div>
    )
}
export default Thailand
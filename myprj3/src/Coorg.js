import React, { useState } from 'react';
import './placesincountries.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';

function Coorg(args) {
 
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const book = () => alert("Booking Successful")

    const imageUrls = [
        'https://www.holidify.com/images/cmsuploads/compressed/attr_wiki_2074_20190221184613jpg',
    ];
    return (
        <div>
            {/* Images */}
            <div className='topdata'>
                <div className="image-container">
                    <h1>Coorg</h1> <br></br>
                    {imageUrls.map((url, index) => (
                        <img key={index} src={url} alt={`Image ${index + 1}`}
                            style={{ width: '50%', height: '5%' }} />
                    ))}
                </div>
            </div>

            <br></br>

            {/* Data in points */}
            <div className='locationdata'>
                <h5><i class="fa-solid fa-location-dot"></i>  Karnataka  India</h5>
                <h5><i class="fa-solid fa-indian-rupee-sign"></i> Packages :  5,000 onwards </h5>
                <h5><i class="fa-regular fa-star"></i> Rating :  4.2 /5 (2023 ratings)</h5>
                <h5><i class="fa-solid fa-cloud"></i> Weather :  16 - 24°C </h5>
                <h5><i class="fa-solid fa-calendar-days"></i> Ideal duration : 2-3 days</h5>
                <h5> <i class="fa-solid fa-clock"></i> Best Time : September - June </h5>
            </div>

            <br></br>

            {/* Booking button */}
            <div>
                <div>
                    <Button color="danger" onClick={toggle}>
                        Book Now
                    </Button>
                    <Modal isOpen={modal} toggle={toggle} {...args}>
                        <ModalHeader toggle={toggle}>Hotel Booking</ModalHeader>
                        <ModalBody>


                            <Form>
                                <FormGroup>
                                    <Label for="namefull">
                                        Full Name
                                    </Label>
                                    <Input
                                        id="namefull"
                                        name="Full Name"
                                        placeholder="Enter your name here"
                                        type="text"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="noofpersons">
                                        No of persons
                                    </Label>
                                    <Input
                                        id="noofpersons"
                                        name="No of persons"
                                        placeholder="Enter number of persons to book room"
                                        type="text"
                                    />
                                </FormGroup>

                                <FormGroup check>
                                    <Input type="checkbox" />
                                    {' '}
                                    <Label check>
                                        Check me out
                                    </Label>
                                </FormGroup>
                                <Button onClick={book}>
                                    <Link to="/">Book Now</Link>
                                </Button>
                            </Form>

                        </ModalBody>
                    </Modal>
                </div>
            </div>

            <br></br>

            {/* Place description */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>"The Scotland of India"</h4>
                    <h6>Coorg Tourism</h6> <br></br>
                    <p>Located amidst imposing mountains in Karnataka with a perpetually misty landscape, Coorg is a popular coffee producing hill station. It is popular for its beautiful green hills and the streams cutting right through them. It also stands as a popular destination because of its culture and people. The Kodavas, a local clan specializing in martial arts, are especially notable for their keen hospitality.</p>
                    <p>Coorg, officially known as Kodagu, is the most affluent hill station in Karnataka. It is well known for its breathtakingly exotic scenery and lush greenery. Forest covered hills, spice and coffee plantations only add to the landscape. Madikeri is the region's centre point with all transportation for getting around starting from here. On a visit to Coorg, cover the beautiful towns like Virajpet, Kushalnagar, Gonikoppal, Pollibetta, and Somwarpet, and experience the beautiful concept of "homestays" to make your experience more memorable! </p>


                </div>
            </div>

            <br></br>

            {/* Must Know Main Points */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>Must Know Before You Travel to Coorg</h4>

                    <p>
                    <li>Some outdoor attractions may be closed or inaccessible during the monsoon season due to heavy rains.</li>
                   <li>During the peak season (October to March) and long weekends,  it's advisable to book accommodations in advance.</li>
                    </p>




                    <h6>Scotland of India </h6>
                    <p>
                    Coorg was called Scotland of India by the Britishers during their visit in the 19th century to this quaint little town of Karnataka. Once you enter the geographical boundaries of this hill town, you will realise the place has done true justice to its nickname. Coorg is famous for the surreal beauty of nature and the hills of the Western Ghats covered by the mist of clouds and accompanied by the pleasant weather. To enjoy the best views that Coorg has to offer, you must visit Raja's Seat - a small garden adorned with the flowers affording a view of the majestic hills of the Western Ghats. Mandalpatti viewpoint is another spot which offers the awe-gaping view of the lush green beauty of Coorg along with the experience of trekking amid the hills of Western Ghats.
                    </p>


                    <h6>Kodava Culture</h6>
                    <p>
                    The Kodavas (a local clan in Coorg), are considered to be absolutely the most hospitable people. What makes Coorg as a tourist destination better, is the fact that the Kodavas are markedly different from other cultures. For instance, it is said that attending a Kodava marriage, wherein the ceremony is conducted not by a priest but by the elders of the two families, is considered as an enriching experience. The marriage takes place after the cutting of banana stumps by a representative. Dancing commences on tribal beats and instruments post the feast. The concept of "Kanyadaan" is absent. The Kodavas also happen to be different because of their different dressing style. For instance, the women wear their saris in a different way wherein the pleats are tucked at the back, and the loose end of the sari is drawn under the left shoulder and secured over the right. This community also happens to be animal and ancestor worshippers, and have a number of festivals celebrated, the most important of which are: Puthari (harvest festival), Kail Polud (celebrating end of the paddy transplanting season), and Kaveri Sankramana (celebrating the birth of the river Kaveri). The Kodavas, a martial arts race, lay a lot of importance on their weapons, and hence have a festival dedicated to them - Keil Podh.
                     </p>


                    <h6>Coffee Plantations</h6>
                    <p>
                    If there is one thing that defines Coorg the best, then it has to be the infinite regal fields of coffee plantations. Coorg is one of the highest producers of coffee in India. You must visit the Tata plantation trails of coffee in Coorg and enjoy the freshly brewed coffee made from the fresh cocoa beans. To gain the best experience you can also choose to stay at one of the resorts located right in the centre of coffee fields. The coffee grown in Coorg is apparently the best mild coffee in the world, as it is grown in the shade. Coorg is popular for the plantation of 'Arabica' (which requires an altitude of 3,300 feet to 4,900 feet above the sea level), and 'Robusta' (requiring an altitude of 1,600 feet to around 3,300 feet above the sea level). It is necessary for coffee to be grown around a lot of trees and shade, as it would prevent soil erosion. Coorg has about 270 species of trees listed. An ideal rain of 60 to 80 inches is required for the plantation of coffee. Very cold weather is not suitable for the growth of coffee.  The temperature ideal for the growth of coffee is between 23 degrees Celsius, and 28 degree Celsius.
                     </p>


                    <h6>Restaurants and Local Food in Coorg </h6>
                    <p> 
                        
                        <li>Coorg is the home of authentic Kodava cuisine. Like most areas in and around the town, the platter revolves around the staple rice. A speciality of the area is Sannakki, a fragrant kind of rice. Non-vegetarian preparations of pork, meat and fish are also popular.</li>
                       
                       <li>Other local favorites include Akki Oti (Rice chapatis), a huge variety of Pattus (a steamed dish), Pulvas, preparations of pork, dried fish, crab meat, Baimbale Curry (a preparation of Bamboo Shoots), Kumm Curry (a preparation of wild mushrooms) and Chekke curry (made using unripe jackfruit). Also find a wide range of teasing chutneys here, as well as unique pickles of pork, fish, mushrooms, bitter orange and tender bamboo.</li>
                        
                        <li>A number of restaurants also serve many vegetarian dishes along with typical Indian, Chinese, Continental and Tibetan cuisines as well as popular South-Indian cuisine consisting of dishes like dosa, vada, idli, sambhar and rasam.</li>
                    </p>

                    <h6>Food of Coorg</h6>
                    <p>
                        <li>Coorg is the home of authentic Kodava cuisine. Like most areas in and around the town, the platter revolves around the staple rice. A speciality of the area is Sannakki, a fragrant kind of rice. Non-vegetarian preparations of pork, meat and fish are also popular.</li>
                    <li>Other local favorites include Akki Oti (Rice chapatis), a huge variety of Pattus (a steamed dish), Pulvas, preparations of pork, dried fish, crab meat, Baimbale Curry (a preparation of Bamboo Shoots), Kumm Curry (a preparation of wild mushrooms) and Chekke curry (made using unripe jackfruit). Also find a wide range of teasing chutneys here, as well as unique pickles of pork, fish, mushrooms, bitter orange and tender bamboo.</li>
                    <li>A number of restaurants also serve many vegetarian dishes along with typical Indian, Chinese, Continental and Tibetan cuisines as well as popular South-Indian cuisine consisting of dishes like dosa, vada, idli, sambhar and rasam.</li>
                    

                    
                     </p>

                </div>
            </div>


            {/* Back to Home button */}
            <div className='backth'>
            <Link to="/india"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>
            </div>




        </div>







    )
}

export default Coorg
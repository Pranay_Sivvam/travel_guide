import './App.css';
import { Route, Routes, BrowserRouter as Router } from 'react-router-dom';
import RegistrationForm from './RegistrationForm';
import Intro from './Intro';
import LoginComponent from './LoginComponent';
import Navbefore from './Navbefore';
import Lehladakh from './Lehladakh';
import Agra from './Agra';
import Manali from './Manali';
import Coorg from './Coorg';
import AndamanNicobar from './AndamanNicobar';
import Udaipur from './Udaipur';
import Gangtok from './Gangtok';
import Munnar from './Munnar';
import Goa from './Goa';
import Kodaikanal from './Kodaikanal';
import Ooty from './Ooty';
import Alleppey from './Alleppey';
import India from './India';
import Singapore from './Singapore';
import Singaporelocal from './Singaporelocal';
import SentosaIsland from './SentosaIsland';
import Gardensbytheway from './Gardensbytheway';
import Thailand from './Thailand';
import bangkok from './bangkok';
import phuket from './phuket';
import chiangmai from './chiangmai';
import Phiphi from './Phiphi';
import Krabi from './Krabi';
import Ayutthaya from './Ayutthaya';
import Uae from './Uae';
import Malaysia from './Malaysia';
import Srilanka from './Srilanka';
import Oman from './Oman';
import Maldives from './Maldives';
import Nepal from './Nepal';
import Indonesia from './Indonesia';
import Vietnam from './Vietnam';
import Seychelles from './Seychelles';
import Mauritius from './Mauritius';
import Navafter from './Navafter';
import HalongBay from './HalongBay';
import HoiAn from './HoiAn';
import HoChiMinhCity from './HoChiMinhCity';
import Contact from './Contact';

function App() {
  return (
    <Router>
    <div className="App">

        <Routes>
          <Route path='/' exact Component={Navbefore} ></Route>
          <Route path='/navafter' exact Component={Navafter} ></Route>
          <Route path='/intro' exact Component={Intro} ></Route>

          <Route path='/RegistrationForm' Component={RegistrationForm}></Route>
          <Route path='/login' Component={LoginComponent}></Route>
          
          <Route path='/india' Component={India}></Route>
          <Route path='/lehladakh' Component={Lehladakh}></Route>
          <Route path='/agra' Component={Agra}></Route>
          <Route path='/manali' Component={Manali}></Route>
          <Route path='/coorg' Component={Coorg}></Route>
          <Route path='/andaman' Component={AndamanNicobar}></Route>
          <Route path='/udaipur' Component={Udaipur}></Route>
          <Route path='/gangtok' Component={Gangtok}></Route>
          <Route path='/munnar' Component={Munnar}></Route>
          <Route path='/goa' Component={Goa}></Route>
          <Route path='/kodaikanal' Component={Kodaikanal}></Route>
          <Route path='/ooty' Component={Ooty}></Route>
          <Route path='/alleppey' Component={Alleppey}></Route>

          <Route path='/singapore' Component={Singapore}></Route>
          <Route path='/singaporelocal' Component={Singaporelocal}></Route>
          <Route path='/gardens' Component={Gardensbytheway}></Route>
          <Route path='/sentosaisland' Component={SentosaIsland}></Route>

          
          <Route path='/Thailand' Component={Thailand}></Route>
          <Route path='/bangkok' Component={bangkok}></Route>
          <Route path='/phuket' Component={phuket}></Route>
          <Route path='/chiangmai' Component={chiangmai}></Route>
          <Route path='/phiphi' Component={Phiphi}></Route>
          <Route path='/krabi' Component={Krabi}></Route>
          <Route path='/ayutthaya' Component={Ayutthaya}></Route>


          <Route path='/vietnam' Component={Vietnam}></Route>
          <Route path='/holongbay' Component={HalongBay}></Route>
          <Route path='/hoian' Component={HoiAn}></Route>
          <Route path='/hochiminh' Component={HoChiMinhCity}></Route>


          <Route path='/uae' Component={Uae}></Route>
          <Route path='/malaysia' Component={Malaysia}></Route>
          <Route path='/srilanka' Component={Srilanka}></Route>
          <Route path='/oman' Component={Oman}></Route>
          <Route path='/maldives' Component={Maldives}></Route>
          <Route path='/nepal' Component={Nepal}></Route>
          <Route path='/indonesia' Component={Indonesia}></Route>
          <Route path='/seychelles' Component={Seychelles}></Route>
          <Route path='/mauritius' Component={Mauritius}></Route>

<Route path='/contactus' Component={Contact}></Route>

        </Routes>
    
    </div>
 
  </Router>
  );
}

export default App;

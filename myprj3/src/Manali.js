import React, { useState } from 'react';
import './placesincountries.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';

function Manali(args) {

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const book = () => alert("Booking Successful")

    const imageUrls = [
        'https://www.holidify.com/images/compressed/490.jpg?v=1.1',
    ];
    return (
        <div>
            {/* Images */}
            <div className='topdata'>
                <div className="image-container">
                    <h1>Manali</h1> <br></br>
                    {imageUrls.map((url, index) => (
                        <img key={index} src={url} alt={`Image ${index + 1}`}
                            style={{ width: '50%', height: '5%' }} />
                    ))}
                </div>
            </div>

            <br></br>

            {/* Data in points */}
            <div className='locationdata'>
                <h5><i class="fa-solid fa-location-dot"></i>  Himachal Pradesh  India</h5>
                <h5><i class="fa-solid fa-indian-rupee-sign"></i> Packages :  5,000 onwards </h5>
                <h5><i class="fa-regular fa-star"></i> Rating :  4.5 /5 (2023 ratings)</h5>
                <h5><i class="fa-solid fa-cloud"></i> Weather :  8 - 17°C </h5>
                <h5><i class="fa-solid fa-calendar-days"></i> Ideal duration : 2-4 days</h5>
                <h5> <i class="fa-solid fa-clock"></i> Best Time : October - June </h5>
            </div>

            <br></br>

            {/* Booking button */}
            <div>
                <div>
                    <Button color="danger" onClick={toggle}>
                        Book Now
                    </Button>
                    <Modal isOpen={modal} toggle={toggle} {...args}>
                        <ModalHeader toggle={toggle}>Hotel Booking</ModalHeader>
                        <ModalBody>


                            <Form>
                                <FormGroup>
                                    <Label for="namefull">
                                        Full Name
                                    </Label>
                                    <Input
                                        id="namefull"
                                        name="Full Name"
                                        placeholder="Enter your name here"
                                        type="text"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="noofpersons">
                                        No of persons
                                    </Label>
                                    <Input
                                        id="noofpersons"
                                        name="No of persons"
                                        placeholder="Enter number of persons to book room"
                                        type="text"
                                    />
                                </FormGroup>

                                <FormGroup check>
                                    <Input type="checkbox" />
                                    {' '}
                                    <Label check>
                                        Check me out
                                    </Label>
                                </FormGroup>
                                <Button onClick={book}>
                                    <Link to="/">Book Now</Link>
                                </Button>
                            </Form>

                        </ModalBody>
                    </Modal>
                </div>
            </div>

            <br></br>

            {/* Place description */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>"Lover's Paradise - India's Honeymoon capital"</h4>
                    <h6>Manali Tourism</h6> <br></br>
                    <p>With spectacular valleys, breathtaking views, snowcapped mountains, and lush forests of oak, deodar, and pine, Manali is a magical hill station at the northern end of Kullu valley in Himachal Pradesh. Gifted to the world by the mighty Himalayas, it is known to be one of the most popular destinations for Honeymooners. Manali offers magnificent views of the Pir Panjal and the Dhauladhar ranges, covered in a thick blanket of snow for most of the year.

                        Over the last few years, Manali has evolved into a place loved by young people looking for more extended stays. With ambient cafes, good wifi availability, small eateries, and convenient shops, Old Manali is among the favorite neighborhood for such people. In addition, many homestays and hostels offer dorm beds for cheap for longer durations.

                        Apart from vacations and workations, Manali is a must-visit for trekkers, as it's an excellent base for exploring this side of the Himalayas. River Beas provides great rafting options in the nearby town of Kullu. Adjoining the Parvati river lies the Parvati Valley, with  Kasol, Manikaran, Tosh, and small villages attracting travelers for extended stays. Atal Tunnel allows travelers to reach Sissu within a few hours, making Spiti more accessible.

                        Tourists flock to Rohtang Pass & Solang valley for various adventure activities, including skiing, paragliding, horse-riding & zorbing. It snows the most in January & February, followed by December and March. If you're fortunate, you might find some snow in April.</p>


                </div>
            </div>

            <br></br>

            {/* Must Know Main Points */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>Must Know Before You Travel to Manali</h4>

                    <p>
                    <li>Due to unpredictable weather conditions, adventure sports like Paragliding are bound to get cancelled at the last moment, ensure to check cancellation charges and other details with notable operators. </li>
                    <li>Children below 12 years of age are not allowed to participate in adventure activities.</li>
                    <li>Due to heavy snowfall, the Rohtang pass remains closed from November to April. </li>
                    <li>You'll need a permit to enter Sethan Valley, which is available at Hydro Project check post in Prini for INR 100.</li>
                    <li>Tourists are advised to check the weather conditions during monsoons (June to August) as the region is susceptible to landslides.</li>
                    </p>




                    <h6>Nightlife in Manali</h6>
                    <p>
                        <li>After reading through the rocky Himalayas all day, Manali offers some of the best ways to enjoy a tranquil nightlife. From the relaxing aromas of Cafes to groovy Bars and Lounges, this beautiful hill station is full of vigour and energy. Dine, party and enjoy away the night time with your friends in exotic stations such as Johnson's bar and restaurant or Zing Zing Bar. If you want to spend some downtime with your partner in low laid Discotheques, The Chelsea club and Buzz are some of the top choices. The local markets and sidewalks are also excellent spots for catching a glimpse of traditional night routines of locals, stargazing around the circuit street parks is also a serenading experience.</li>
                    </p>


                    <h6>Adventure Activities in Manali </h6>
                    <p>
                        Many tourists enjoy adventure activities in Manali. It is important to keep a few things in mind. Wear clothes which allow the harness to be wound securely. They should not be too tight or too loose. Avoid visiting the place during rainy season. Here are some more detailed tips for your adventure!
                        <li>Accomodation - Manali has been one of a major holiday destination in India especially for North-Indians. The place is full of accommodations of all types, from basic and medium range guest houses and hotels to luxurious resorts. There is also an option of camping in the beautiful grounds of Manali.</li>
                        <li>Medical Fitness - Watch your respiratory system. If you have breathing issues and any heart related problem, please do not undertake this activity.</li>
                        <li>Safety - Choose schools which are certified and offer good safety gear. Before starting the activity, make sure your safety gear is in place. Notify your instructor in case of any glitches or discomfort.</li>
                    </p>


                    <h6>Restaurants and Local Food in Manali  </h6>
                    <p>
                        Manali is a hill station glittering with amazing restaurants, cafes and bars, that an take care of any need, desires or cravings that its visitors may have. You will find countless restaurants with a rich variety and some of the most delicious food on their menus. You can find Italian, Chinese, Korean, Continental, Indian, Japanese, Thai, Vietnamese cuisines here along with the popular Tibetan Momos
                        The cafes with a parallel culture of their own, cater to younger crowds. These cafes serve pizzas, momos, banana pancakes and apple pies throughout the day and you can also try Yak's cheese here. Along with these do relish the street food there with Samosas, Aloo tikki, Bread Pakoras, Paav Bhaji, Gulab Jamun and more filling the streets with colors and fragrances. Other than these, the city has an equally rich platter of local Himachal food.
                    </p>


                    <h6>Suggested Itinerary for Manali </h6>
                    <p> 
                        Day 1 :
                        <li>Reach Manali by morning. Have breakfast and relax for a while. After freshening up, you can start your tour with the Hadimba Temple. After this, you can head out to the Mall road for some grub and a little bit of shopping. The main market is bustling with activity and you will find a number of items from woollen clothes, shawls, local handicrafts and everything you need.</li>
                       Day 2 :
                       <li>You should head off to Rohtang Pass. It remains open from April to November. It is a two-hour road journey from Manali and you need to set off early in the morning around 4:00 in order to avoid the traffic. The entry passes are available in Manali and you need to get it a day in advance. While returning, you should stop over at Solang Valley.</li>
                        Day 3 :
                        <li>You can also go to Vashisht hot springs. Soak yourself in the hot water springs in the winters and it feels magical. After this, you can head to Old Manali for shopping and a scrumptious lunch.</li>
                        Day 4 : 
                        <li>While returning, stopover at Bhuntar and you can go for rafting in the season. Otherwise, you can also explore Kullu's market and temples. There are a number of things you can do, depending on your mood and the season.</li>
                
                    
                    </p>

                    
                    <h6>What is the best way to reach Manali?  </h6>
                    <p>
                    The nearest railway station from Manali is in Joginder Nagar which is 50 km away. However, this is a narrow gauge railway station and hence is not connected to all the major cities in the country. The broad gauge railway stations nearest to Manali are in Chandigarh (310 km) and Ambala (300 km). The nearest airport is in Bhuntar, which is also 50 km away. Buses and taxis are easily available from these places and other parts of North India to Manali. Manali can also be reached from Leh by the Leh-Manali route, though it is open for only 4 or 5 months in a year during summers when the snow is removed by the Border Road Organization of India. Still, this road is very picturesque and is one of the most beautiful ways to reach Manali.
                    </p>





                </div>
            </div>


            {/* Back to Home button */}
            <div className='backth'>
            <Link to="/india"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>
            </div>




        </div>







    )
}

export default Manali
import React from 'react'
// import 'bootstrap/dist/css/bootstrap.css';
import './IntroPage.css';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom'
import './App.css'
import IntroPage from './IntroPage';
import { useSpring, animated } from 'react-spring';
import Footer from './Footer';
import Home from './Home';
import Blog from './Blog';
import Contact from './Contact';
import Aboutme from './Aboutme';


function Navbefore() {
    
    const fadeIn = useSpring({
        opacity: 1,
        from: { opacity: 0 },
        config: { duration: 1000 },
      });
    
    
    
    return (
        <div>

{/* <Aboutme/> */}
{/* <Home/> */}
{/* <Blog/> */}
{/* <Contact/> */}




         <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div>
                    <img className='logoimgs' src='Logo.png' alt='RoamRover' />
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">


                    <li class="nav-item active">
                            <button className='btn btn-dark my-2 my-sm-0' id='spcbtn' ><Link to="/login">About us</Link></button>
                        </li>

                        <li class="nav-item active" id='spcbtn'>
                            <button className='btn btn-dark my-2 my-sm-0' ><Link to="/contactus">Contact us</Link></button>
                        </li>

                       

                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <button class="btn btn-dark my-2 my-sm-0" type="submit"><Link to="/login">Login</Link></button>
                        <button class="btn btn-dark my-2 my-sm-0" type="submit"><Link to="/RegistrationForm">RegistrationForm</Link></button>
                    </form>
                </div>
            </nav>
           
            <animated.div className="intro-container" style={fadeIn}>
      <h1 className='head'>RomeRover</h1> <br></br>
      <p className='fontincc'>Your passport to unparalleled travel experiences!</p>
      <p className='fontincc'>Embark on a journey where every destination is a stroke of wanderlust waiting to be painted.</p>
      <br></br>
      <Link to="/login"><button className='linkclr'>Explore Now</button></Link>
      
    </animated.div>

<br></br>

    {/* <Footer/> */}
 

            <br></br>

        </div>

    )
}

export default Navbefore
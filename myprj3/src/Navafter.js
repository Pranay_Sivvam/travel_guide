import React from 'react'
// import 'bootstrap/dist/css/bootstrap.css';
import { BrowserRouter, Route, Switch, Link} from 'react-router-dom'
import './App.css'
import IntroPage from './IntroPage'
import Intro from './Intro'
import Home from './Home'
import Footer from './Footer'

function Navafter() {
    return (
        <div>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div>
                <Link to="/navafter"><img className='logoimg' src='Logo.png' alt='RoamRover' /></Link>
                    
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <button className='btn btn-dark my-2 my-sm-0' id='spcbtn'><Link to="/login">Home</Link></button>
                        </li>
                        <li class="nav-item active">
                            <button className='btn btn-dark my-2 my-sm-0' id='spcbtn' ><Link to="/login">Blog</Link></button>
                        </li>
                      
                        <li class="nav-item active">
                            <button className='btn btn-dark my-2 my-sm-0' id='spcbtn' ><Link to="/login">About us</Link></button>
                        </li>
                        <li class="nav-item active" id='spcbtn'>
                            <button className='btn btn-dark my-2 my-sm-0' ><Link to="/contactus">Contact us</Link></button>
                        </li>

                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown"  role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Destinations
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#"><Link to="/india">India</Link></a>
                                <a class="dropdown-item" href="#"><Link to="/singapore">Singapore</Link></a>
                                <a class="dropdown-item" href="#"><Link to="/Thailand">Thailand</Link></a>
                                <a class="dropdown-item" href="#"><Link to="/vietnam">Vietnam</Link></a>
                                <a class="dropdown-item" href="#"><Link to="/uae">UAE</Link></a>
                                <a class="dropdown-item" href="#"><Link to="/malaysia">Malaysia</Link></a>
                                <a class="dropdown-item" href="#"><Link to="/srilanka">Srilanka</Link></a>
                                <a class="dropdown-item" href="#"><Link to="/oman">Oman</Link></a>
                                <a class="dropdown-item" href="#"><Link to="/maldives">Maldives</Link></a>
                                <a class="dropdown-item" href="#"><Link to="/nepal">Nepal</Link></a>
                                <a class="dropdown-item" href="#"><Link to="/indonesia">Indonesia</Link></a>
                                <a class="dropdown-item" href="#"><Link to="/seychelles">Seychelles</Link></a>
                                <a class="dropdown-item" href="#"><Link to="/mauritius">Mauritius</Link></a>

                            </div>
                        </li>

                    </ul>
                   
                </div>
            </nav>
            <div>

            </div>
            <br></br>
            {/* <Intro /> */}
            <Home/>
            <Footer/>
        
        </div>

    )
}

export default Navafter
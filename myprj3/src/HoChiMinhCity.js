import React, { useState } from 'react';
import './placesincountries.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';

function HoChiMinhCity(args) {
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const book = () => alert("Booking Successful")

    const imageUrls = [
        'https://www.holidify.com/images/bgImages/HO-CHI-MINH-CITY.jpg',
    ];


    return (
        <div>
            {/* Images */}
            <div className='topdata'>
                <div className="image-container">
                    <h1>Ho Chi Minh City</h1> <br></br>
                    {imageUrls.map((url, index) => (
                        <img key={index} src={url} alt={`Image ${index + 1}`}
                            style={{ width: '50%', height: '5%' }} />
                    ))}
                </div>
            </div>

            <br></br>

            {/* Data in points */}
            <div className='locationdata'>
                <h5><i class="fa-solid fa-location-dot"></i> Union Territory - Vietnam</h5>
                <h5><i class="fa-solid fa-indian-rupee-sign"></i> Packages :  17,501 onwards</h5>
                <h5><i class="fa-regular fa-star"></i> Rating :  3.0/5 (2023 ratings)</h5>
                <h5><i class="fa-solid fa-cloud"></i> Weather : 24 - 32°C </h5>
                <h5><i class="fa-solid fa-calendar-days"></i> Ideal duration : 2-3 day</h5>
                <h5> <i class="fa-solid fa-clock"></i> Best Time :  December-February.</h5>
            </div>

            <br></br>

            {/* Booking button */}
            <div>
                <div>
                    <Button color="danger" onClick={toggle}>
                        Book Now
                    </Button>
                    <Modal isOpen={modal} toggle={toggle} {...args}>
                        <ModalHeader toggle={toggle}>Hotel Booking</ModalHeader>
                        <ModalBody>


                            <Form>
                                <FormGroup>
                                    <Label for="namefull">
                                        Full Name
                                    </Label>
                                    <Input
                                        id="namefull"
                                        name="Full Name"
                                        placeholder="Enter your name here"
                                        type="text"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="noofpersons">
                                        No of persons
                                    </Label>
                                    <Input
                                        id="noofpersons"
                                        name="No of persons"
                                        placeholder="Enter number of persons to book room"
                                        type="text"
                                    />
                                </FormGroup>

                                <FormGroup check>
                                    <Input type="checkbox" />
                                    {' '}
                                    <Label check>
                                        Check me out
                                    </Label>
                                </FormGroup>
                                <Button onClick={book}>
                                <Link to="/">Book Now</Link>
                                </Button>
                            </Form>

                        </ModalBody>
                    </Modal>
                </div>
            </div>

            <br></br>

            {/* Place description */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>"Erstwhile Saigon and now a thriving metropolis with dynamic energy and historical landmarks"</h4>
                    <h6>Ho Chi Minh City Tourism</h6> <br></br>
                    <p>Popularly known as Saigon, Ho Chi Minh City in southern Vietnam is the largest city and the financial and cultural capital of the country. Ho Chi Minh City is a fast-paced city deeply rooted in its cultural roots whilst quickly having adapted to the demands of urbanisation. Here you will find ornate temples and olden shrines of various faiths standing side by side alongside towering skyscrapers and shopping malls. Tan Son Nhat International Airport, which is the busiest airport in the country services Ho Chi Minh City.

                         Overcoming a turbulent period in its past with its peak at the Vietnam war, Ho Chi Minh City has since grown leaps and bounds to become arguably the most iconic tourist destination in the country. There are several historical attractions such as the Cu Chi and Ben Dinh Tunnels which are remnants of its war-torn past. Other places of interest include the Notre Dame Cathedral, Saigon Opera House and Central Post Office which were built during the rule of the French and brilliantly showcase classical French architecture. These prominent tourist spots are located in District 1 of the city, all close to each other.

                         Local markets such as Ben Thanh and Binh Tay offer visitors a wide range of handicrafts, textiles, electronics and bric-a-brac souvenirs. Catering to more modern and lavish tastes, Ho Chi Minh City is home to many eccentric shopping malls, fine dining restaurants and glitzy nightclubs as well.   </p>


                </div>
            </div>

            <br></br>

            {/* Must Know Main Points */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>Nightlife in Ho Chi Minh City </h4>
                    <p>
                        The city of Ho Chi Minh has a dazzling nightlife lined with an array of pubs and bars ranging from dingy ones to murky dark places and posh and premium properties.

                        <li>Most of the popular ones are dotted in the Dong Khoi area of District 1. </li>

                        <li>Then there are tearooms and bia hoi draught beer vendors that operate until late.</li>

                        <li>If jazz music is your taste, do not miss out on the Sax n' Art at 28 Le Loi Street, where the renowned Vietnamese saxophone artist Tran Manh Tuan frequents.</li>

                        <li>One can also enjoy music shows and other grand events at Lan Anh stadiums. Drinks and alcohol are readily available along with many local drinks, which are relatively cheaper compared to the imported brands.</li>

                        <li>If you are looking for a fantastic experience into the wee hours of the night, Pham Ngu Lao in the district has plenty of cheap beer vendors waiting to quench your thirst.</li>

                        

                    </p>

                </div>
            </div>


            {/* Back to Home button */}
            <div className='backth'>
            <Link to="/vietnam"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>
                
            </div>




        </div>
    )
}

export default HoChiMinhCity
import React, { useState } from 'react';
import './placesincountries.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';


function SentosaIsland(args) {
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const book = () => alert("Booking Successful")

    const imageUrls = [
        'https://www.holidify.com/images/cmsuploads/compressed/1_sentosa_aerial_2016_20190127112531.jpg',
    ];


    return (
        <div>
            {/* Images */}
            <div className='topdata'>
                <div className="image-container">
                    <h1>Sentosa Island</h1> <br></br>
                    {imageUrls.map((url, index) => (
                        <img key={index} src={url} alt={`Image ${index + 1}`}
                            style={{ width: '50%', height: '5%' }} />
                    ))}
                </div>
            </div>

            <br></br>

            {/* Data in points */}
            <div className='locationdata'>
                <h5><i class="fa-solid fa-location-dot"></i> Singapore </h5>
                <h5><i class="fa-solid fa-indian-rupee-sign"></i> Packages :  18,500 onwards</h5>
                <h5><i class="fa-regular fa-star"></i> Rating :  4.7 /5 (2023 ratings)</h5>
                <h5><i class="fa-solid fa-cloud"></i> Weather : 25 - 32°C </h5>
                <h5><i class="fa-solid fa-calendar-days"></i> Ideal duration : 3-4 days</h5>
                <h5> <i class="fa-solid fa-clock"></i> Best Time : February - April </h5>
            </div>

            <br></br>

            {/* Booking button */}
            <div>
                <div>
                    <Button color="danger" onClick={toggle}>
                        Book Now
                    </Button>
                    <Modal isOpen={modal} toggle={toggle} {...args}>
                        <ModalHeader toggle={toggle}>Hotel Booking</ModalHeader>
                        <ModalBody>


                            <Form>
                                <FormGroup>
                                    <Label for="namefull">
                                        Full Name
                                    </Label>
                                    <Input
                                        id="namefull"
                                        name="Full Name"
                                        placeholder="Enter your name here"
                                        type="text"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="noofpersons">
                                        No of persons
                                    </Label>
                                    <Input
                                        id="noofpersons"
                                        name="No of persons"
                                        placeholder="Enter number of persons to book room"
                                        type="text"
                                    />
                                </FormGroup>

                                <FormGroup check>
                                    <Input type="checkbox" />
                                    {' '}
                                    <Label check>
                                        Check me out
                                    </Label>
                                </FormGroup>
                                <Button onClick={book}>
                                    <Link to="/">Book Now</Link>
                                </Button>
                            </Form>

                        </ModalBody>
                    </Modal>
                </div>
            </div>

            <br></br>

            {/* Place description */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>"The Quintessential Cosmopolitan"</h4>
                    <h6>Sentosa Island Tourism</h6> <br></br>
                    <p>A tiny resort-island off the southern coast of Singapore, Sentosa Island is home to themed attractions, sun-washed beaches, lush rainforests, amazing spas and some popular resort hotels.

                        Packed with attractions, the tiny island is speckled with panoramic gardens including a butterfly garden, amusement parks, a breath-taking aquarium (Underwater World), beach clubs with lively music, an ethnic village and the highlight of the island, a vast Merlion Statue.

                        Singapore's only preserved fort, Fort Siloso, is also located on Sentosa Island. Siloso Beach is for those seeking solace at the beach with volleyball. The place can be easily approached by hired cars, bus, cable car or even Sentosa Express that costs SGD 4.</p>


                </div>
            </div>

            <br></br>

            {/* Must Know Main Points */}
            <div className='toalighdata'>
                <div className='placedesc'>
                    <h4>Must Know Before You Travel to Sentosa Island</h4>
                    <p>
                        Sentosa Island Tourist Pass (STP) :
                        <li>It is a special EZ-Link stored-value card allowing unlimited travel in MRT and Bus for one, two, and three days. It can be bought at TransitLink Ticket Office at selected MRT stations.
                            Children above 0.9m in height and below 7 years can travel free on MRT and bus. Apply for a Child Concession Card at TransitLink Ticket Offices.</li>

                        Tax Refund :
                        <li>If you purchase anything above USD 100 (including GST), you can claim a 7% GST refund. Look out for the Tax Free shopping logo or a sign displayed at the shop you are buying from. You can have 3 same-day receipts to meet the minimum purchase requirement of USD 100. The refund can be claimed via Electronic Tourist Refund (eTRS) self-help kiosk at the airport or via Global Blue or Tourego Mobile Applications (App).</li>

                        Wifi & Connectivity :
                        <li>There is a free public Wifi service across Sentosa Island. Download the Wireless@SGx App to auto connect.</li>
                        No Smoking Areas:
                        <li>Smoking is not allowed in air-conditioned places, and your tobacco products should not exceed 4gm. Smoke only at designated smoking areas.</li>

                    </p>


                    <h6>Exchanging Money in Sentosa Island </h6>
                    <p>
                        <li>Currency exchange in Sentosa Island is done in banks as well as currency exchange booths found in almost every shopping mall. Generally, these booths offer better rates, opening hours and much faster service than banks.</li>
                        <li>The Mustafa Mall, which is open 24 hours, accepts almost any currency at a very good rate with equal competitiveness as the small shops located at Change Alley next to Raffles Place MRT.</li>
                        <li>Rates at the airport are not as good as in the city, so avoid exchanging there.</li>
                        <li>The ATMs are easily available throughout Sentosa Island in large numbers.</li>
                    </p>

                    <h6>Currency in Sentosa Island</h6>
                    <p>
                        <li>Sentosa Island's unit of currency is the Sentosa Island Dollar (SGD), locally referred to as the 'Singdollar'. It is made up of 100 cents using coins of 5, 10, 20 and 50 cents, along with notes in the denomination of SGD 2, 5, 10 and 50, 100, 500 and 1000. </li>
                        <li>Cards: Almost all the major credit card brands are widely accepted in Sentosa Island including Visa and MasterCard (although a 3 % surcharge may be charged by some shops, taxis may charge up to 15 %).
                            Traveller's cheques are usually not accepted, however, can be cashed at most of the exchange booths.
                            EZ-Link and Nets Flash Pay cards are valid in case of some convenience stores and fast-food chains.</li>
                        <li>ATMs: ATMs are widely available at banks, malls, MRT stations and commercial areas.</li>
                    </p>


                    <h6>Daily Budget for Sentosa Island </h6>
                    <p>
                        Sentosa Island, in general, is an expensive country by the standards of the Asian countries. Although the city caters to the taste and interest of each kind of tourist, the minimum budget for backpackers, mid-rangers and luxury travellers are higher in comparison to the neighbouring countries. The cost of travelling in Sentosa Island per day and per person (excluding hotel expense) is:
                        <li>Budget Travel: Less than SGD 200</li>
                        <li>Comfortable Travel: SGD 200 - 400</li>
                        <li>Luxury Travel: SGD 400 plus</li>
                    </p>

                    <h6>Religion of Sentosa Island </h6>
                    <p>
                        Being a multi-religious country, Sentosa Island does not have a state-regulated religion which the citizens are supposed to follow. It is home to 10 religions, out of which Buddhism, Hinduism, Islam and Christianity are the primary religions, while Zoroastrianism, Judaism, Sikhism, Jainism and others form the minority cluster. The Lion City is the ultimate melting pot, with the locals celebrating all festivals pompously, irrespective of the religion they follow.

                    </p>

                    <h6>Language of Sentosa Island</h6>
                    <p>
                        <li>There are four official languages of Sentosa Island: English, Malay, Mandarin Chinese and Tamil. Yes, Sentosa Island language is as diverse and multi-cultural as its people! English is the most widely spoken language (primarily by the population below the age of 50), and the medium of instructions in school. English is also the language of business and government in Sentosa Island, based on British English. </li>
                        <li>A unique and widely spoken language in Sentosa Island is the Singlish. It is primarily the colloquial form of English, having a distinct accent, and ignoring the basic standards of English grammar. Having a jumble of local slang and expressions of various languages and dialects of Sentosa Island, speaking in Singlish is seen as a mark of being truly local! </li>
                        <li>The major portion of the literate population in Sentosa Island is bilingual, with English and Mandarin being most commonly spoken. Interestingly, all the schools in the city teach the language of the child's parentage, along with English, to ensure the child stay in touch with the traditional roots.</li>
                    </p>


                    <h6>History of Sentosa Island</h6>
                    <p>
                        <li>According to the historical records, the story of Sentosa Island goes as back as the third century, then referred to as 'island at the end'. It was between 1298 and 1299 AD that the first settlement was established when the city came to be known as Tumasik or Sea Town. According to a legend, the current name of the country came into being during the 14th century when a prince of Palembang (capital of Srivijaya) saw an animal he had never seen before while on his hunting trip. Taking it as a good omen, he named the city as 'Singapura' or the 'lion city' derived from the Sanskrit words 'Simha' meaning 'lion' and 'pura' meaning.</li>
                        <li>Soon, the city located at the tip of the Malay Peninsula (natural meeting point of various sea routes) became the major trading hub for Arabs dhows, Chinese junk, Portuguese battleships and Buginese schooners. However, the modern city of Sentosa Island was founded in the 19th century by Sir Thomas Stanford Raffles. After passing from various hands of power, the island was handed over to British in 1946 as its crown colony. The revolution for independence came about in 1959 when the country's first general elections were held and Lee Kuan Yew became first Prime Minister of Sentosa Island. One can witness the imprints of the island's rich past and struggle at various museums, monuments and memorials.</li>

                    </p>


                </div>
            </div>


            {/* Back to Home button */}
            <div className='backth'>
                <Link to="/singapore"><button className='btn'><i class="fa-solid fa-house fa-2xl"></i></button></Link>
            </div>




        </div>
    )
}
export default SentosaIsland